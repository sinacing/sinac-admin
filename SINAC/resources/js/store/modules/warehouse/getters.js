const getWarehouses = state=> state.warehouses;
const getLoading = state=> state.isLoading;

export default {
    getWarehouses,
    getLoading
}