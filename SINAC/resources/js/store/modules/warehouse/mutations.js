const getWarehouses = (state,payload)=>{
    state.status = payload.status;
    state.warehouses = payload.data.warehouses;
    state.isLoading = false;
}

const storeWarehouse = (state,payload)=>{
    state.warehouses = [...state.warehouses, payload.data.warehouse];
    state.message = payload.data.message;
    state.status = payload.status;
    state.isLoading = false;
}


const updateWarehouse = (state,payload)=>{
    let result = payload.result;
    let objWarehouse = payload.data;

    Object.assign(state.warehouses[objWarehouse.indexWarehouse], result.data.warehouse);
    state.message = result.data.message;
    state.status = result.status;
    state.isLoading = false;
}

const deleteWarehouse = (state,payload)=>{
    state.isLoading = false;
    state.warehouses.splice(payload.data.indexWarehouse,1);
    state.message = payload.result.data.message;
    state.status =  payload.result.status;
}

const getWarehouseIndex = (state,warehouse)=>{
    state.findWarehouseIndex = state.warehouses.indexOf(warehouse)
}


const handleErrors = (state,err,data)=>{
    state.errors = err.data.errors;
    state.status = err.status;
    state.isLoading = false;
    state.message = err.data.message;
    console.log(err);
   
}

export default {
    getWarehouses,
    storeWarehouse,
    updateWarehouse,
    deleteWarehouse,

    handleErrors,
    getWarehouseIndex
}