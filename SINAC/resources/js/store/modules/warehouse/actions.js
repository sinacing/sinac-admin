import { apiWarehouse } from '../../../services';

const getWarehouses = (context)=>{
    apiWarehouse.getListWarehouseApi()
    .then(response => context.commit('getWarehouses',response))
    .catch(err => context.commit('handleErrors',err.response))
}

const storeWarehouse = (context,warehouse)=>{
    apiWarehouse.storeWarehouseApi(warehouse)
    .then(result => context.commit('storeWarehouse',result))
    .catch(err => context.commit('handleErrors',err.response))
}

const updateWarehouse = (context,data)=>{
    apiWarehouse.updateWarehouseApi(data.warehouse)
    .then(result => context.commit('updateWarehouse',{data,result}))
    .catch(err => context.commit('handleErrors',err.response))
}

const deleteWarehouse = (context,data)=>{
    apiWarehouse.deleteWarehouseApi(data.warehouse)
    .then(result => context.commit('deleteWarehouse',{data,result}))
    .catch(err => context.commit('handleErrors',err.response))
}

const getWarehouseIndex = (context,warehouse)=>{
    context.commit('getWarehouseIndex',warehouse)
}

export default {
    getWarehouses,
    storeWarehouse,
    updateWarehouse,
    deleteWarehouse,
    getWarehouseIndex
}