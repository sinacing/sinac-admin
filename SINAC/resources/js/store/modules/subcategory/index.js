import actions from './actions';
import getters from './getters';
import mutations from './mutations';


const state = {
    subcategories: [],
    errors:null,
    findSubcategoryIndex:-1,
    message:null,
    status:null,
    isLoading:true
}

export default{
    namespaced: true,
    state,
    actions,
    getters,
    mutations,
}