const getSubcategories = state=> state.subcategories;
const getLoading = state=> state.isLoading
export default {
    getSubcategories,
    getLoading
}