import { apiSubCategory } from '../../../services';

const getSubcategories = (context)=>{
    apiSubCategory.getListSubCategoryApi()
    .then(response => context.commit('getSubcategories',response))
    .catch(err => context.commit('handleErrors',err.response))
}

const storeSubcategory = (context,subcategory)=>{
    apiSubCategory.storeSubCategoryApi(subcategory)
    .then(result => context.commit('storeSubcategory',result))
    .catch(err => context.commit('handleErrors',err.response));
}

const updateSubcategory = (context,data)=>{
    apiSubCategory.updateSubCategoryApi(data.subcategory)
    .then(result => context.commit('updateSubcategory',{result,data}))
    .catch(err => context.commit('handleErrors',err.response));
}

const deleteSubcategory = (context,data)=>{
    apiSubCategory.deleteSubCategory(data.subcategory)
    .then(result =>context.commit('deleteSubcategory',{data,result}))
    .catch(err => context.commit('handleErrors',err.response))
}

const getSubcategoryIndex = (context,category)=>{
    context.commit('getSubcategoryIndex',category);
}

export default {
    getSubcategories,
    storeSubcategory,
    updateSubcategory,
    deleteSubcategory,
    getSubcategoryIndex,

}