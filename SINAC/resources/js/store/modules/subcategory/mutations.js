const getSubcategories = (state,payload)=>{
    state.status = payload.status;
    state.subcategories = payload.data.subcategories;
    state.isLoading = false
}


const storeSubcategory = (state,payload)=>{
    
    state.subcategories = [...state.subcategories, payload.data.subcategory]
    state.status = payload.status
    state.isLoading = false
    state.message = payload.data.message
}


const updateSubcategory = (state,payload)=>{
    let data = payload.data;
    let dataResp = payload.result;

    Object.assign(state.subcategories[data.indexSubcategory],dataResp.data.subcategory);
    state.status = dataResp.status;
    state.message = dataResp.data.message;
    state.isLoading = false;
}

const handleErrors = (state,err)=>{
    state.errors = err.data.errors;
    state.isLoading = false;
    state.status = err.status;
    state.message = err.data.message;
}

const deleteSubcategory = (state,payload)=>{
    state.isLoading = false;
    state.subcategories.splice(payload.data.indexSubcategory,1);
    state.message = payload.result.data.message;
    state.status =  payload.result.status;
}

const getSubcategoryIndex = (state,subcategory)=>{
    state.findSubcategoryIndex = state.subcategories.indexOf(subcategory);
}

export default {
    getSubcategories,
    storeSubcategory,
    updateSubcategory,
    deleteSubcategory,
    handleErrors,
    getSubcategoryIndex,
}