const getCategories = (state,payload)=>{
    state.status = payload.status;
    state.categories = payload.data.categories;
    state.isLoading = false;
}

const storeCategory = (state,payload)=>{
    state.status = payload.status;
    state.categories = [...state.categories,payload.data.category];
    state.message = payload.data.message;
    state.isLoading = false;
}

const handleErrors = (state,err)=>{
    state.errors = err.data.errors;
    state.isLoading = false;
    state.status = err.status;
    state.message = err.data.message;
}

const updateCategory = (state,payload)=>{
    let data = payload.data;
    let dataResp = payload.response;

    Object.assign(state.categories[data.indexCategory],dataResp.data.category);
    state.status = dataResp.status;
    state.message = dataResp.data.message;
    state.isLoading = false;
}

const deleteCategory = (state,payload)=>{
    state.isLoading = false;
    state.categories.splice(payload.data.indexCategory,1);
    state.message = payload.result.data.message;
    state.status =  payload.result.status;
}

const getCategoryIndex = (state,category)=>{
    state.findCategoryIndex = state.categories.indexOf(category);
}

export default {
    getCategories,
    storeCategory,
    getCategoryIndex,
    handleErrors,
    updateCategory,
    deleteCategory,
}