import { apiCategory } from '../../../services';

const getCategories = (context)=>{
    apiCategory.getListCategoryApi()
    .then(response => context.commit('getCategories',response))
    .catch(err => context.commit('handleErrors',err.response))
}

const storeCategory = (context,data)=>{
    apiCategory.storeCategoryApi(data)
    .then(response => context.commit('storeCategory',response))
    .catch(err => context.commit('handleErrors',err.response))
}

const updateCategory = (context,data)=>{
    apiCategory.updateCategoryApi(data.category)
    .then(response => context.commit('updateCategory',{data,response}))
    .catch(err => context.commit('handleErrors',err.response))
}

const deleteCategory = (context,data)=>{
    apiCategory.deleteCategoryApi(data.category)
    .then(result => context.commit('deleteCategory',{result,data}))
    .catch(err => context.commit('handleErrors',err.response))
}

const getCategoryIndex = (context,category)=>{
    context.commit('getCategoryIndex',category);
}

export default {
    getCategories,
    getCategoryIndex,
    storeCategory,
    updateCategory,
    deleteCategory,
}