import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const state = {
    categories: [],
    errors:null,
    findCategoryIndex:-1,
    message:null,
    status:null,
    isLoading:true
}

export default{
    namespaced: true,
    state,
    actions,
    getters,
    mutations,
}