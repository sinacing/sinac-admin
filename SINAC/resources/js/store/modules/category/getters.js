const getCategories = state=> state.categories;
const getLoading = state => state.isLoading
export default {
    getCategories,
    getLoading
}