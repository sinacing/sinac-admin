const getStatus = state => state.statuses;
const getUserStatus = state => state.userStatus;
const getActiveStatus = state => state.activeStatus;
const getArticleStatus = state => state.userStatus;
const getSupplierStatus = state =>state.supplierStatus;

export default {
    getStatus,
    getUserStatus,
    getActiveStatus,
    getArticleStatus,
    getSupplierStatus
}