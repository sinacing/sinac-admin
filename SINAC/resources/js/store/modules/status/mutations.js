const getStatus = (state,payload)=>{
    let status =  payload.data.statuses;
    state.statuses = status;
    state.status = payload.status;
    state.isLoading = -1;
    filterStatusModule(state,status);
}

const filterStatusModule = (state,status)=>{
    // user
    state.userStatus = status.slice(0,2);
    // active
    state.activeStatus = status.slice(2,5);
    // article
    state.articleStatus = status.slice(0,2);
    //supplier
    state.supplierStatus = status.slice(0,2);
}


const handleError = (state,err)=>{
    console.log(err)
}

export default{
    getStatus,
    handleError
}