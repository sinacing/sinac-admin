import { apiStatus } from '../../../services';

const getStatus = (context)=>{
    apiStatus.getStatusApi()
    .then((result) => context.commit('getStatus',result))
    .catch((err) => context.commit('handleError',err.response))
}

export default{
    getStatus
}