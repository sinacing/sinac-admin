const getUsers = (state,payload)=>{
    state.status = 200;
    state.users = payload.data.users;
    state.isLoading = false;
}


const storeUser = (state,payload)=>{
    let user = payload.data.user;

    state.status = payload.status;
    state.message = payload.data.message; 
    state.users = [...state.users,user];
    state.isLoading = false;
}


const updateUser = (state,payload)=>{
    let userResponse = payload.response.data.user;
    
    state.message = payload.response.data.message;
    Object.assign(state.users[findUserIndex(state,payload.user.id)],userResponse);
    state.status = payload.response.status;
    state.isLoading = false;
}

const disabledUser = (state,payload)=>{
    let indexUser = state.users.indexOf(payload.data.user);
    Object.assign(state.users[indexUser],payload.resp.data.user);
    state.status = payload.resp.status;
    state.isLoading = false
}

const filterUserSuccess = (state,payload)=>{
    state.status = payload.status;
    state.isLoading = false;
    state.users = payload.data.users;
}


const deleteUserSuccess = (state,payload) =>{
    let dataUser = payload.data;
    let response = payload.response;

    state.isLoading = false;
    state.message = response.data.message;
    state.users.splice(dataUser.indexDelete,1);
    state.status = response.status;
}

const findUserIndex = (state,userId)=>{
    let user = state.users.filter( user => userId == user.id);
    return state.users.indexOf(user[0]);
}

const handleError = (state,err)=>{
    console.log(err,'user');
    state.isLoading = false;
    state.status = err.status;
    state.errors = err.data.errors;
    state.message = "Han ocurrido errores";
}

export default {
    getUsers,
    storeUser,
    updateUser,
    disabledUser,
    filterUserSuccess,
    findUserIndex,
    deleteUserSuccess,
    handleError
}