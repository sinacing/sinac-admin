const getUsers = state => state.users;
const getLoading = state => state.isLoading

export default{
    getUsers,
    getLoading
}