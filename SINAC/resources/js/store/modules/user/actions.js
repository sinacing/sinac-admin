import { apiUser } from '../../../services'

const getUsers = (context)=>{
    apiUser.getListUserApi()
    .then(response=>context.commit('getUsers',response))
    .catch(err=> context.commit('handleError',err.response))
}

const storeUser = (context,user)=>{
    apiUser.storeUserApi(user) // debo mandar pedir los datos al backend del usuario y devolverlos
    .then(response=> context.commit('storeUser',response))
    .catch(err => context.commit('handleError',err.response))
}

const updateUser = (context,user)=>{
    apiUser.updateUserApi(user)
    .then(response=> context.commit('updateUser',{user,response}))
    .catch(err => context.commit('handleError',err.response))
}

const disabledUser = (context,data)=>{
    apiUser.disableUserApi(data)
    .then(resp => context.commit('disabledUser',{resp,data}))
    .catch(err => context.commit('handleError',err.response))
}

const filterUser = (context,valueFilter)=>{
    apiUser.filterUserApi(valueFilter)
    .then(resp => context.commit('filterUserSuccess',resp))
    .catch(err => context.commit('handleError',err.response))
}

const deleteUser = (context,data)=>{
    
    apiUser.deleteUserApi(data.user)
    .then(response => context.commit('deleteUserSuccess',{data,response}))
    .catch(err => context.commit('handleError',err.response))
}

export default{
    getUsers,
    storeUser,
    updateUser,
    disabledUser,
    filterUser,
    deleteUser
}