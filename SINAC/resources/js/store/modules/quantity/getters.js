const getQuantity = state => state.quantities;
const getTypeCommodities = state =>state.typeCommodities;
const getUnitMeasures = state=> state.unitMeasures;

export default {
    getQuantity,
    getTypeCommodities,
    getUnitMeasures
}