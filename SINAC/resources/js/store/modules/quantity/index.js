import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const state = {
    quantities:[],
    typeCommodities:[],
    unitMeasures:[],
    isLoading:true,
    status:200,
    message:null
}

export default {
    namespaced: true,
    state,
    actions,
    getters,
    mutations
}