const getQuantitiesSuccess= (state,data)=>{
    state.status = 200;
    state.quantities = data.quantities;
    state.isLoading = false;
}

const getQuantitiesFail = (state,err)=>{
    state.status = 500;
    state.message = err;
}

const getTypeCommoditySuccess = (state,payload)=>{
    state.typeCommodities = payload.data.typecommodities;
    state.isLoading = false;
    state.status = payload.status;
}

const getTypeCommoditiesFail = (state,err)=>{
    console.log(err)
}

const getUnitMeasureSuccess = (state,payload)=>{
    state.unitMeasures = payload.data.unitmeasures;
    state.isLoading = false;
    state.status = payload.status;
}

const getUnitMeasuresFail = (state,err)=>{
    console.log(err)
}

export default{
    getQuantitiesSuccess,
    getQuantitiesFail,
    getTypeCommoditySuccess,
    getTypeCommoditiesFail,
    getUnitMeasureSuccess,
    getUnitMeasuresFail
}