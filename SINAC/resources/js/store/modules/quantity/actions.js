import { apiQuantity, apiTypeCommodity, apiUnitMeasure } from '../../../services';

const getQuantity = (context)=>{
    apiQuantity.getQuantityApi()
    .then(response => context.commit('getQuantitiesSuccess',response))
    .catch(err => context.commit('getQuantitiesFail',err.response))
}

const getTypeCommodity = (context)=>{
    apiTypeCommodity.getTypeCommodityApi()
    .then(response => context.commit('getTypeCommoditySuccess',response))
    .catch(err => context.commit('getTypeCommoditiesFail',err.response))
}

const getUnitMeasure = (context)=>{
    apiUnitMeasure.getUnitMeasureApi()
    .then(response => context.commit('getUnitMeasureSuccess',response))
    .catch(err => context.commit('getUnitMeasuresFail',err.response))
}

export default {
    getQuantity,
    getTypeCommodity,
    getUnitMeasure,
}