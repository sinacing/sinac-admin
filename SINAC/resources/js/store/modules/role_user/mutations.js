const getRoleUsersSuccess = (state,payload)=>{
    state.role_users = payload.data.role_users;
    state.status = payload.status;
    state.isLoading = false;
}

const modifyRoleUserSuccess = (state,payload)=>{
    // console.log(payload.resp,payload.roles)

    let indexRoleUser = state.role_users.indexOf(payload.roles.user);
    Object.assign(state.role_users[indexRoleUser],payload.resp.data.role[0])
    state.status = payload.resp.status;
    state.message = payload.resp.data.message;
    state.isLoading = false;
}

const handleErrors = (state,err)=>{
    console.log(err)
}

// Roles
const getRolesSuccess = (state,payload)=>{
    state.roles = payload.data.roles;
    state.status = payload.status;
    state.isLoading = false;

}

export default {
    getRoleUsersSuccess,
    handleErrors,
    getRolesSuccess,
    modifyRoleUserSuccess
}