import { apiRoleUser,apiRole } from '../../../services'

const getRoleUsers = context=>{
    apiRoleUser.getRoleUserListApi()
    .then(resp => context.commit('getRoleUsersSuccess',resp))
    .catch(err => context.commit('handleErrors',err.response))
}
// Roles
const getRoles = context=>{
    apiRole.getRolesApi()
    .then(resp => context.commit('getRolesSuccess',resp))
    .catch(err => context.commit('handleErrors',err.response))
}

const modifyRoleUser = (context,roles)=>{
   apiRoleUser.modifyRoleUserApi(roles)
   .then(resp => context.commit('modifyRoleUserSuccess',{resp,roles}))
   .catch(err => context.commit('handleErrors',err.response))
}   


export default {
    getRoleUsers,
    getRoles,
    modifyRoleUser
}


// Nota: En caso futuro si se desea tener el control sobre el manejo de los roles, se debera crear un store para dicho control
// En este caso solo se obtendran la lista de roles con el metodo getRoles()