import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const state = {
    role_users:[],
    roles:[],
    isLoading:true,
    status:200,
    message:null,
    errors:''
}

export default {
    namespaced: true,
    state,
    actions,
    getters,
    mutations
}