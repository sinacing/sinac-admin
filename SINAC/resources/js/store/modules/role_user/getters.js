const getRoleUsers = state => state.role_users;
// roles
const getRoles = state=> state.roles;

export default {
    getRoleUsers,
    getRoles
}