import { apiPersonSupplier } from '../../../services';

const getPersonSuppliers = (context)=>{
    apiPersonSupplier.getPersonSupplierApi()
    .then(response => context.commit('getPersonSuppliers',response))
    .catch(err => context.commit('handleErrors',err.response))
}

const storePersonSupplier = (context,personSupplier)=>{
    apiPersonSupplier.storePersonSupplierApi(personSupplier)
    .then(resp => context.commit('storePersonSupplier',resp))
    .catch(err => context.commit('handleErorrs',err.response))
}

const updatePersonSupplier = (context,personSupplier)=>{
    apiPersonSupplier.updatePersonSupplierApi(personSupplier)
    .then(resp => context.commit('updatePersonSupplier',{personSupplier, resp}))
    .catch(err => context.commit('handleErorrs',err.response))
}


const disabledPersonSupplier = (context,data)=>{
    apiPersonSupplier.disablePersonSupplierApi(data.query)
    .then(resp => context.commit('disabledPersonSupplier',{resp,user:data.user}))
    .catch(err => context.commit('handleErorrs',err.response))
}


export default {
    getPersonSuppliers,
    storePersonSupplier,
    updatePersonSupplier,
    disabledPersonSupplier
}