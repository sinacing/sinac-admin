import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const state = {
    personSuppliers: [],
    message: null,
    isLoading: true,
    status:null,
    errors:null,
    findPersonSupplierIndex:-1,
}

export default{
    namespaced:true,
    state,
    actions,
    getters,
    mutations
}