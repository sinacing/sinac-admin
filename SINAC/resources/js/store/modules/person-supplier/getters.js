const getPersonSuppliers = state=> state.personSuppliers;
const getLoading = state => state.isLoading;

export default {
    getPersonSuppliers,
    getLoading
}