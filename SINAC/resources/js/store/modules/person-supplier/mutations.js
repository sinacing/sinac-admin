const getPersonSuppliers = (state,payload)=>{
    state.isLoading = false;
    state.personSuppliers = payload.data.person_supplier;
    state.status = payload.status;
}

const storePersonSupplier = (state,payload)=>{
    state.status = payload.status;
    state.personSuppliers = [...state.personSuppliers, payload.data.person_supplier];
    state.message = payload.data.message;
    state.isLoading = false;
}

const updatePersonSupplier = (state,payload)=>{
    let supplier = payload.resp.data.person_supplier;
    Object.assign(state.personSuppliers[findPersonSupplierIndex(state,payload.personSupplier.id)],supplier)
    state.status = payload.resp.status;
    state.message = payload.resp.data.message;
    state.isLoading = false;
}

const disabledPersonSupplier = (state,payload)=>{
    let indexSupplier = state.personSuppliers.indexOf(payload.user);
    Object.assign(state.personSuppliers[indexSupplier],payload.resp.data.person_supplier);
    state.status = payload.resp.status;
    state.isLoading = false
}

const findPersonSupplierIndex = (state, id)=>{
    let personSupplier = state.personSuppliers.filter(ps => ps.id == id)
    return state.personSuppliers.indexOf(personSupplier[0]) 
}

const handleErorrs = (state,err)=>{
    console.log(err)
    state.message = "Ha ocurrido un error";
    state.errors = err.data.errors
    state.isLoading = false;
    state.status = err.status;
}

export default {
    getPersonSuppliers,
    handleErorrs,
    storePersonSupplier,
    updatePersonSupplier,
    disabledPersonSupplier
}