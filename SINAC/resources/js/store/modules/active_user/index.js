import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const state = {
    activesAsigned: [],
    message: null,
    isLoading: true,
    status:null,
    errors:null,
    findActiveAsignedIndex:-1,
}

export default{
    namespaced:true,
    state,
    actions,
    getters,
    mutations
}