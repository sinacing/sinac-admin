const getListActiveAsigned = (state,payload)=>{
    state.isLoading = false;
    state.activesAsigned = payload.data.activesAsigned;
    state.status = payload.status;
}

const storeActiveAsigned = (state,payload)=>{
    state.status = payload.status;
    state.isLoading = false;
    state.activesAsigned = [...state.activesAsigned, payload.data.activeAsigned];
    state.message = payload.data.message;
}

const deallocateActive = (state,payload)=>{
    let activesUser = state.activesAsigned.filter(active => active.id !== payload.id)
    state.activesAsigned = activesUser;
    state.status = payload.resp.status;
    state.message = payload.resp.data.message;
    state.isLoading = false;
    
}

const handleErorrs = (state,err)=>{
    state.message = "Ha ocurrido un error";
    state.errors = err.data.errors.active_id[0]
    state.isLoading = false;
    state.status = err.status;
}


export default{
    getListActiveAsigned,
    storeActiveAsigned,
    deallocateActive,
    handleErorrs
}