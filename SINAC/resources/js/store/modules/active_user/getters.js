const getActiveAsignedList = state => state.activesAsigned;   
const getLoading = state=> state.isLoading

export default{
    getActiveAsignedList,
    getLoading

}