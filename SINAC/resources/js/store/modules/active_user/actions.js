import { apiActiveUser } from '../../../services'

const getActiveAsignedList = (context)=>{
    apiActiveUser.getListActiveAsignedApi()
    .then(resp => context.commit('getListActiveAsigned', resp))
    .catch(err =>context.commit('handleErorrs',err.response))
}

const storeActiveAsigned = (context,activeAsigned)=>{
    apiActiveUser.storeActiveAsignedApi(activeAsigned)
    .then(resp => context.commit('storeActiveAsigned',resp))
    .catch(err => context.commit('handleErorrs',err.response))
}

// TODO Test this action 
const deallocateActiveUser = (context,id)=>{
    apiActiveUser.deallocateActiveApi(id)
    .then(resp => context.commit('deallocateActive',{resp,id}))
    .catch(err => context.commit('handleErrors', err.response))
}

const getActiveAsignedIndex = (context,activeAsigned)=>{
    context.commit('getActiveAsignedIndex',activeAsigned);
}

export default {
    getActiveAsignedList,
    storeActiveAsigned,
    getActiveAsignedIndex,
    deallocateActiveUser
}