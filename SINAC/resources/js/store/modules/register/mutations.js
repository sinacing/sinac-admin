const getRegistersSuccess = (state,payload)=>{

    state.registers = payload.data.registers;
    state.status = payload.status;
    state.isLoading = false
}

const storeRegisterSuccess = (state,payload)=>{
    state.status = payload.status
    state.message = payload.data.message
    state.errors = payload.data.errors
    state.isLoading = false;
}   

const actionConfirmSuccess = (state,payload)=>{
    state.isLoading = false;
    state.message = payload.resp.data.message
    let indexRegister = state.registers.indexOf(payload.data)
    state.registers.splice(indexRegister,1)
}


const handleErrors = (state,err)=>{
    console.log(err)
    state.status = err.status;
    state.isLoading = false
    state.errors = err.data.errors
}

export default {
    getRegistersSuccess,
    storeRegisterSuccess,
    actionConfirmSuccess,
    handleErrors
}