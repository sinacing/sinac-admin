import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const state = {
    registers:[],
    isLoading:true,
    status:200,
    message:null,
    errors:''
}

export default {
    namespaced: true,
    state,
    actions,
    getters,
    mutations
}