import { apiRegister } from '../../../services' 

const getRegisters = context=>{
    apiRegister.getRegistersApi()
    .then(resp => context.commit('getRegistersSuccess',resp))
    .catch(err => context.commit('handleErrors',err.response))
}

const storeRegister = (context,data)=>{
    apiRegister.storeRegisterApi(data)
    .then(resp => context.commit('storeRegisterSuccess',resp))
    .catch(err => context.commit('handleErrors',err.response))
}

const acceptedRegister = (context,data)=>{
    apiRegister.acceptedRegisterApi(data)
    .then(resp => context.commit('actionConfirmSuccess',{data,resp}))
    .catch(err => context.commit('handleErrors',err.response))
}

const deleteRegister = (context,data)=>{
    apiRegister.deleteRegisterApi(data)
    .then(resp => context.commit('actionConfirmSuccess',{data,resp}))
    .catch(err => context.commit('handleErrors',err.response))
}

export default {
    getRegisters,
    storeRegister,
    acceptedRegister,
    deleteRegister
}