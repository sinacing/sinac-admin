const getCommoditySuccess= (state,payload)=>{
    state.status = payload.status;
    state.commodities = payload.data.commodities;
    state.isLoading = false;
}

const storeCommoditySuccess = (state,payload)=>{
    state.isLoading = false;
    state.status = payload.status;
    state.commodities = [...state.commodities, payload.data.commodity];
    state.message = payload.data.message;
}

const updateCommodity = (state,payload)=>{
    let data = payload.data;
    let resp = payload.response;

    Object.assign(state.commodities[data.indexArticle],resp.data.commodity);
    state.message = resp.data.message;
    state.status = resp.status
    state.isLoading = false;
}

const disabledArticle = (state,payload)=>{

    let article = state.commodities.filter(art=> payload.data.article == art.id)
    let indexArticle = state.commodities.indexOf(article[0]);
    Object.assign(state.commodities[indexArticle],payload.resp.data.commodity);
    state.status = payload.resp.status;
    state.isLoading = false
}

const filterCommoditySuccess = (state,payload)=>{
    state.status = payload.status;
    state.isLoading = false;
    state.commodities = payload.data.commodities;
}

const findIndexArticle = (state,commodity)=>{
    state.findCommodityIndex = state.commodities.indexOf(commodity);
}

const handleError = (state,err)=>{
    console.log(err);
    state.isLoading = false;
    state.status = err.status;
    state.errors = err.data.errors;
    state.message = "Han ocurrido errores";
}

export default {
    getCommoditySuccess,
    storeCommoditySuccess,
    updateCommodity,
    findIndexArticle,
    disabledArticle,
    filterCommoditySuccess,
    handleError
}