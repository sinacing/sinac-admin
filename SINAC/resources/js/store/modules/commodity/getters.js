const getCommodity = state => state.commodities;
const getFindCommodity = state => state.findCommodity;
const findCommodityIndex = state => state.findCommodityIndex;
const getLoading = state=>state.isLoading

export default{
    getCommodity,
    getFindCommodity,
    findCommodityIndex,
    getLoading
}