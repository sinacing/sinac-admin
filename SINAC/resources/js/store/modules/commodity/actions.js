import { apiCommodity } from '../../../services'

const getCommodity = (context)=>{
    apiCommodity.getListCommodityApi()
    .then(response=>context.commit('getCommoditySuccess',response))
    .catch(err=> context.commit('handleError',err.response))
}

const storeCommodity = (context,commodity)=>{
    apiCommodity.storeCommodityApi(commodity) // debo mandar pedir los datos al backend del usuario y devolverlos
    .then(response=> context.commit('storeCommoditySuccess',response))
    .catch(err => context.commit('handleError',err.response))
}

const updateCommodity = (context,data)=>{
    apiCommodity.updateCommodityApi(data.article)
    .then(response=> context.commit('updateCommodity', {data,response}))
    .catch(err => context.commit('handleError',err.response))
}

const disabledCommodity = (context,data)=>{
    apiCommodity.disableCommodityApi(data)
    .then(resp => context.commit('disabledArticle',{resp,data}))
    .catch(err => context.commit('handleError',err.response))
}

const filterCommodity = (context,valueFilter)=>{
    apiCommodity.filterCommodityApi(valueFilter)
    .then(resp => context.commit('filterCommoditySuccess',resp))
    .catch(err => context.commit('handleError',err.response))
}

const findIndexArticle = (context,article)=>{
    context.commit('findIndexArticle',article);
}

export default{
   getCommodity,
   storeCommodity,
   updateCommodity,
   findIndexArticle,
   disabledCommodity,
   filterCommodity
}