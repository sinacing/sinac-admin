import { apiSale } from '../../../services';

const getSales = (context)=>{
    apiSale.getListSaleApi()
    .then(result => context.commit('getSaleSuccess',result))
    .catch(err => context.commit('handleErrors',err.response))
}

const storeSale = (context,sale)=>{
    apiSale.storeSaleApi(sale)
    .then(resp => context.commit('storeSaleSuccess',{sale,resp}))
    .catch(err => context.commit('handleErrors',err.response))
}

export default{
    getSales,
    storeSale
}