const getSaleSuccess = (state,data)=>{
    state.sales = data.sales;

    state.status = 200;
    
}

const storeSaleSuccess = (state,payload)=>{
    console.log(payload)
}

const handleErrors = (state,err)=>{
    console.log(err);
    state.status = 500;
    state.message = err;
}

export default{
    getSaleSuccess,
    storeSaleSuccess,
    
    handleErrors
}