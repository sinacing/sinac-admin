import { apiArea } from '../../../services';

const getAreas = (context)=>{
    apiArea.getListAreaApi()
    .then(response => context.commit('getAreas',response))
    .catch(err => context.commit('handleErrors',err))
}

const storeArea = (context,data)=>{
    apiArea.storeAreaApi(data)
    .then(response => context.commit('storeArea',response))
    .catch(err => context.commit('handleErrors',err.response))
}

const updateArea = (context,data)=>{
    apiArea.updateAreaApi(data.area)
    .then(response => context.commit('updateArea',{data,response}))
    .catch(err => context.commit('handleErrors',err.response))
}

const deleteArea = (context,area)=>{
    apiArea.deleteAreaApi(area)
    .then(resp => context.commit('deleteArea',{resp,area}))
    .catch(err=>context.commit('handleErrors',err.response))
}

const getAreaIndex = (context,area)=>{
    context.commit('getAreaIndex',area);
}

export default {
    getAreas,
    storeArea,
    updateArea,
    deleteArea,
    getAreaIndex,
}