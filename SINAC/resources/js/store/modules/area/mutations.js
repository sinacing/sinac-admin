
const getAreas = (state,payload)=>{
    state.areas = payload.data.areas;
    state.isLoading = false;
    state.status = payload.status;
}

const storeArea = (state,payload)=>{
    state.status = payload.status;
    state.areas = [...state.areas,payload.data.area];
    state.message = payload.data.message;
    state.isLoading = false;
}

const updateArea = (state,payload)=>{
    let data = payload.data;
    let dataResp = payload.response;

    Object.assign(state.areas[data.indexArea],dataResp.data.area);
    state.status = dataResp.status;
    state.message = dataResp.data.message;
    state.isLoading = false;
}


const deleteArea = (state,payload)=>{

    let indexElement = state.areas.indexOf(payload.area)
    state.areas.splice(indexElement,1);
    state.message = payload.resp.data.message;
    state.status =  payload.resp.status;
    state.isLoading = false;

}

const handleErrors = (state,err)=>{
    state.errors = err.data.errors;
    state.isLoading = false;
    state.status = err.status;
    state.message = err.data.message;
}

const getAreaIndex = (state,area)=>{
    state.findAreaIndex = state.areas.indexOf(area);
}

export default{
    getAreas,
    storeArea,
    handleErrors,
    updateArea,
    deleteArea,
    getAreaIndex
}