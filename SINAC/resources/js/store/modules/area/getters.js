const getAreas = state => state.areas;
const getLoading = state=> state.isLoading

export default {
    getAreas,
    getLoading
}
