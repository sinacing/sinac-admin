const getListActive = (state,payload)=>{
    state.isLoading = false;
    state.actives = payload.data.actives;
    state.status = payload.status; 
}


const storeActive = (state,payload)=>{
    state.status = payload.status;
    state.isLoading = false;
    state.actives = [...state.actives, payload.data.active];
    state.message = payload.data.message;
    
}

const updateActive = (state,payload)=>{
    let data = payload.data;
    let resp = payload.resp;
    state.isLoading = false;
    Object.assign(state.actives[data.index],resp.data.active)
    state.status = resp.status;
    state.message = resp.data.message;

}   

const findIndexActive = (state,id)=>{
    let active = state.actives.filter(act => id == act.id);    
    state.findCurrentIndex = state.actives.indexOf(active[0]);
}
const deleteActive = (state,payload) =>{
    let dataActive = payload.data;
    let response = payload.response;
    
    state.isLoading = false;
    state.message = response.data.message;
    state.actives.splice(dataActive.indexDelete,1);
    state.status = response.status;
}

const disabledActive = (state,payload)=>{

    let active = state.actives.filter(act=> payload.data.active == act.id)
    let indexActive = state.actives.indexOf(active[0]);
    Object.assign(state.actives[indexActive],payload.resp.data.actives);
    state.status = payload.resp.status;
    state.isLoading = false;
}
const filterActive = (state,payload)=>{
    state.status = payload.status;
    state.isLoading = false;
    state.actives = payload.data.actives;
}

const handleError = (state,err)=>{
    console.log(err);
    state.isLoading = false;
    state.status = err.status;
    state.errors = err.data.errors;
    state.message = "Han ocurrido errores " + err.data.message;
}

export default{
    getListActive,
    
    storeActive,
    
    updateActive,
    
    findIndexActive,
    deleteActive,
    
    disabledActive,
    handleError,
    filterActive
}