import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const state = {
    actives: [],
    message: null,
    isLoading: true,
    status:null,
    errors:null,
}

export default{
    namespaced:true,
    state,
    actions,
    getters,
    mutations
}