import { apiActives } from '../../../services'

const getActiveList = (context)=>{
    
    apiActives.getListActiveApi()
    .then(resp => context.commit('getListActive', resp))
    .catch(err =>context.commit('handleError',err.response))
}

const storeActive = (context,active)=>{
    apiActives.storeActiveApi(active)
    .then(resp => context.commit('storeActive',resp))
    .catch(err => context.commit('handleError',err.response))
}

const updateActive = (context,data)=>{
    apiActives.updateActiveApi(data.active)
    .then(resp => context.commit('updateActive',{data,resp}))
    .catch(err => context.commit('handleError',err.response))
}

const findIndexActive = (context,id)=>{
    context.commit('findIndexActive',id);
}

const removeActive = (context,payload)=>{
    payload.store.dispatch('active_user/storeActiveAsigned',{user_id:payload.data.user_id,active_id:payload.data.active_id.id});
}

const deleteActive = (context,data)=>{
    apiActives.deleteActiveApi(data.active)
    .then(response => context.commit('deleteActive',{data,response}))
    .catch(err => context.commit('handleError',err.response))
}
const disabledActive = (context,data)=>{
    apiActives.disableActiveApi(data)
    .then(resp => context.commit('disabledActive',{resp,data}))
    .catch(err => context.commit('handleError',err.response))
}

const filterActive = (context,valueFilter)=>{
    apiActives.filterActiveApi(valueFilter)
    .then(resp => context.commit('filterActive',resp))
    .catch(err => context.commit('handleError',err.response))
}

export default {
    getActiveList,
    storeActive,
    updateActive,
    findIndexActive,
    deleteActive,
    disabledActive,
    filterActive,
    removeActive
}