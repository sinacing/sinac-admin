const getActiveList = state => state.actives;   
const findCurrentIndex = state=> state.findCurrentIndex;
const getLoading = state=> state.isLoading
// const getFindActive= state => state.findActive;
// const findActiveIndex = state => state.findActiveIndex;

export default{
    getActiveList,
    findCurrentIndex,
    getLoading
    // getFindActive,
    // findActiveIndex
}