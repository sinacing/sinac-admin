const getCompanySuppliers = (state,payload)=>{
    state.isLoading = false;
    state.companySuppliers = payload.data.company_supplier;
    state.status = payload.status;
}

const handleErorrs = (state,err)=>{
    state.message = "Ha ocurrido un error";
    state.errors = err
    state.isLoading = false;
    state.status = err.status;
}

export default{
    getCompanySuppliers,
    handleErorrs
}