const getCompanySuppliers = state=> state.companySuppliers;
const getLoading = state => state.isLoading;

export default {
    getCompanySuppliers,
    getLoading
}