import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const state = {
    companySuppliers: [],
    message: null,
    isLoading: true,
    status:null,
    errors:null,
    findCompanySupplierIndex:-1,
}

export default{
    namespaced:true,
    state,
    actions,
    getters,
    mutations
}