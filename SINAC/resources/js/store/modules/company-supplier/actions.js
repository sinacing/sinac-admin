import { apiCompanySupplier } from '../../../services';

const getCompanySuppliers = (context)=>{
    apiCompanySupplier.getCompanySupplierApi()
    .then(response => context.commit('getCompanySuppliers',response))
    .catch(err => context.commit('handleErrors',err.response))
}

const getCompanySupplierIndex = (context,companySupplier)=>{
    context.commit('getCompanySupplierIndex',companySupplier);
}

export default {
    getCompanySuppliers,
    getCompanySupplierIndex
}