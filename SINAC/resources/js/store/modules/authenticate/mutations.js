const loginSuccess = (state,result)=>{
    
    state.loggedIn = true;
    state.loading  = false;
    state.auth_error = false;
    state.status = 200
    state.message = `Bienvenido ${result.user.first_name}`
    state.currentUser = Object.assign({},result.user,{token:result.access_token})
    localStorage.setItem('user', JSON.stringify(state.currentUser));
    localStorage.setItem('purchase_user',JSON.stringify([{id:1,name:'Joseph'},{id:1,name:'Lucas'}]))
}

const loginFailed = (state,err)=>{
    state.loading = false;
    state.status = 400
    state.auth_error = err;
}

const logOut = (state)=>{
  localStorage.removeItem('user')
  localStorage.removeItem('purchase_user')
  state.loggedIn = false;
  state.currentUser = null;
}

export default {
    loginSuccess,
    loginFailed,
    logOut
}
