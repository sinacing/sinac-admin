import actions from './actions';
import getters from './getters';
import mutations from './mutations';

import {getLocalUser} from '../../../helpers/auth';

const user = getLocalUser();

const state = {
    currentUser:user,
    loggedIn: !!user,// (cast in boolean)
    loading: true,
    auth_error: null,
    message:null
}

export default{
    namespaced: true,
    state,
    actions,
    getters,
    mutations,
};