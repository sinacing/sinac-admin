import { apiAuthenticate } from '../../../services'

const login = (context, credentials)=>{
    apiAuthenticate.login(credentials.form)
    .then((response)=>{ 
        context.commit('loginSuccess',response);
    })
    .catch(err=> context.commit('loginFailed',err))
}

const logout = (context)=>{
    context.commit('')
}

export default{
    login,
    logout
}