import { apiManagementCenter } from '../../../services';
const getManagementCenter = (context)=>{
    apiManagementCenter.getListManagement_centerApi()
    .then(response => context.commit('getManagementCenter',response))
    .catch(err => context.commit('handleErrors',err.response))
}
const storeManagementCenter = (context, managementCenter)=>{
    apiManagementCenter.storeManagement_centerApi(managementCenter)
    .then(result => context.commit('storeManagementCenter', result))
    .catch(err => context.commit('handleErrors',err.response))  
}
const updateManagementCenter = (context,center)=>{
    apiManagementCenter.updateManagement_centerApi(center)
    .then(result => context.commit('updateManagementCenter',{center,result}))
    .catch(err => context.commit('handleErrors',err.response))
}

const deleteManagementCenter = (context,data)=>{
    apiManagementCenter.deleteManagement_centerApi(data.managementCenter)
    .then(result => context.commit('deleteManagementCenter',{data,result}))
    .catch(err => context.commit('handleErrors', err.response))
}
export default{
    getManagementCenter,
    storeManagementCenter,
    updateManagementCenter,
    deleteManagementCenter,
}