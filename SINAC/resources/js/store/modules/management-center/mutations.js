const getManagementCenter = (state,payload)=>{

    state.status = payload.status;
    state.managementCenters = payload.data.management_center;
    state.isLoading = false;
}

const storeManagementCenter = (state,payload)=>{
    state.managementCenters = [...state.managementCenters, payload.data.management_center];
    state.message = payload.data.message;
    state.status = payload.status;
    state.isLoading = false;
}
const updateManagementCenter = (state,payload)=>{
    let center = payload.result.data.management_center;
    Object.assign(state.managementCenters[getManagementCenterIndex(state, payload.center.id)], center)
    state.message = payload.result.data.message
    state.status = payload.result.status
    state.isLoading = false

}
const deleteManagementCenter = (state, payload)=>{
    state.isLoading = false;
    state.managementCenters.splice(payload.data.indexManagementCenter,1);
    state.message = payload.result.data.message;
    state.status = payload.result.status;
}

const getManagementCenterIndex = (state,id)=>{
   let center = state.managementCenters.filter(mng => mng.id === id)
   return state.managementCenters.indexOf(center[0])
}

const handleErrors = (state,err,data)=>{
    state.errors = err.data.errors;
    state.status = err.status;
    state.isLoading = false;
    state.message = err.data.message;
    console.log(err);
}
export default{
    getManagementCenter,
    storeManagementCenter,
    updateManagementCenter,
    deleteManagementCenter,

    handleErrors,
}