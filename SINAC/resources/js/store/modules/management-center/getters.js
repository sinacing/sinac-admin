const getManagementCenters = state=> state.managementCenters;
const getLoading = state=> state.isLoading

export default{
    getManagementCenters,
    getLoading
}