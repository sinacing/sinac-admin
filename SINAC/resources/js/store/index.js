import Vue from 'vue'
import Vuex from 'vuex'

/** Modules */
import LoginModule from './modules/authenticate';
import UserModule from './modules/user';
import ActivesModule from './modules/actives';
import StatusModule from './modules/status';
import AreaModule from './modules/area';
import CommodityModule from './modules/commodity';
import CategoryModule from './modules/category';
import SubcategoryModule from './modules/subcategory';
import WarehouseModule from './modules/warehouse';
import QuantityModule from './modules/quantity';
import SaleModule from './modules/sales';
import RegisterModule from './modules/register';
import RoleUserModule from './modules/role_user';
import ActiveUserModule from './modules/active_user';
import PersonSupplierModule from './modules/person-supplier';
import CompanySupplierModule from './modules/company-supplier';
import ManagementCenterModule from './modules/management-center';


/** End Modules */      
Vue.use(Vuex);

export default new Vuex.Store({
    modules:{
        login: LoginModule,
        user: UserModule,
        active: ActivesModule,
        status: StatusModule,
        area: AreaModule,
        commodity: CommodityModule,
        category: CategoryModule,
        subcategory: SubcategoryModule,
        warehouse: WarehouseModule,
        quantity:QuantityModule,
        sale:SaleModule,
        register:RegisterModule,
        role_user:RoleUserModule,
        active_user:ActiveUserModule,
        personSupplier: PersonSupplierModule,
        companySupplier: CompanySupplierModule,
        managementCenter:ManagementCenterModule
    }
})