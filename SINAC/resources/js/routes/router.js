import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'
/** Views */
import User from '../views/User'
import UserRequest from '../components/user/requests/UserRequest'
import UserRoles from '../components/user/roles/Role'
import Register from '../components/auth/register/Register'
import Login from '../components/auth/login/Login'
import Dashboard from '../views/Dashboard'
import Active from '../views/Actives'
import Asigned from '../components/actives/actives_asigned/Actives_asigned'

//import Commodity from '../views/Commodity';
import Category from '../components/commodity/category/Category'
import Subcategory from '../components/commodity/subcategory/Subcategory'
import Warehouse from '../components/commodity/warehouse/Warehouse'
import Article from '../components/commodity/article/Article'
import Area from '../components/area/Area'
import Sale from '../components/commodity/sale/Sale'
import Supplier from '../components/supplier/Supplier'
import ManagementCenter from '../components/managementCenter/ManagementCenter'
/** Components */

Vue.use(Router);

const router = new Router({
    mode:'history',
    routes:[
        {
            path:'/',
            component: Dashboard,
            meta:{ requiresAuth: true }
        },
        {
            path:'/user/employees',
            component: User,
            meta:{ requiresAuth: true}
        },
        {
            path:'/user/requests',
            component: UserRequest,
            meta:{ requiresAuth: true}
        },
        {
            path:'/user/roles',
            component: UserRoles,
            meta:{ requiresAuth: true}
        },
        {
            path:'/active/actives_inventory',
            component: Active,
            meta:{ requiresAuth: true}
        },
        {
            path:'/active/actives_asigned',
            component:Asigned,
            meta:{ requiresAuth: true}
        },
        {
            path:'/commodity/article',
            component: Article,
            meta:{ requiresAuth: true},
        },
        {
            path:'/commodity/category',
            component: Category,
            meta:{ requiresAuth: true}
        },
        {
            path:'/commodity/subcategory',
            component: Subcategory,
            meta:{ requiresAuth: true}
        },
        {
            path:'/commodity/warehouse',
            component: Warehouse,
            meta:{ requiresAuth: true} 
        },
        {
            path:'/area',
            component: Area,
            meta:{ requiresAuth: true} 
        },
        {
            path:'/commodity/sale',
            component: Sale,
            meta:{ requiresAuth: true} 
        },
        {
            path:'/supplier',
            component: Supplier,
            meta:{ requiresAuth: true} 
        },
        {
            path: '/register',
            component: Register
        },
        {
            path: '/login',
            component: Login
        },
        {
            path:'/managementCenter',
            component:ManagementCenter,
            meta:{ requiresAuth: true}
        },
        
        
        // {
        //     path: '*',
        //     component: () => import('@/views/pages/Index'),
        //     children: [
        //       {
        //         name: '404 Error',
        //         path: '',
        //         component: () => import('@/views/pages/Error')
        //       }
        //     ]
        //   }
    ]
})

router.beforeEach((to,from,next)=>{
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
    const currentUser = store.state.login.currentUser;
    if(requiresAuth && !currentUser){ // refactor db,because a dont know what kind of user has permission to navigate on the route
        next('/login') // move another route
    }
    else if(to.path == '/login' && currentUser){
        next('/') //maintain the same route
    }
    else{
        next(); // confirm the route
    }
}) 

export default router;