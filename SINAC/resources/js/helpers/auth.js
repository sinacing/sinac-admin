const getLocalUser = ()=>{
    const userData = localStorage.getItem('user');
    const purchaseData = localStorage.getItem('purchase_user');
    const result = JSON.parse(purchaseData)
    const userStorage = !userData ? null : JSON.parse(userData);
    //console.log(userData,...result )
    return userStorage;
}

export {
    getLocalUser
}