const currentItems = (items,currentPage,itemsPerPage)=>{
    let indexLastPage = currentPage * itemsPerPage
    let indexFirstPage = indexLastPage - itemsPerPage
    return items.slice(indexFirstPage, indexLastPage)
}

const totalPaginates = (numItems, itemsPerPage)=>{
    return Math.ceil(numItems / itemsPerPage) 
}

export {
    currentItems,
    totalPaginates
}