let newObject = [];

const filterObject = (objectList,elementFind)=>{
    if(elementFind !== null && elementFind.length > 2)
    {
        newObject = [];

        for(let i=0; i< objectList.length; i++)
        {
            if(objectList[i].name.toLowerCase().indexOf(elementFind.toLowerCase()) > -1)
            {
                newObject = [...newObject,objectList[i]]
            }
        }
        return newObject.length > 0 ? newObject : 0;
    }
    return 0;
}

export {
    filterObject
}