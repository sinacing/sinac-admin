import Axios from "axios";
import { getLocalUser } from '../helpers/auth';

let axios = Axios;
axios.defaults.headers.common['Content-Type'] = 'application/json';

const getListCommodityApi = async ()=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.get('/api/commodities',{data:{}});
}

const storeCommodityApi = async (commodity)=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.post('/api/commodities',commodity);
} 

const updateCommodityApi = async (commodity)=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.put(`/api/commodities/${commodity.id}`,commodity);
}

const disableCommodityApi = async data=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.post(`/api/commodities/disable`,data)
}

const filterCommodityApi = async valueFilter=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.post('/api/commodities/filterCommodity',{valueFilter});
}

const apiCommodity = {
    getListCommodityApi,
    storeCommodityApi,
    updateCommodityApi,
    disableCommodityApi,
    filterCommodityApi
}

export { 
    apiCommodity,
}