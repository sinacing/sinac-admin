import Axios from "axios";
import { getLocalUser } from '../helpers/auth';

let axios = Axios;
axios.defaults.headers.common['content-type'] = 'application/json';

const getListWarehouseApi = async ()=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.get('/api/warehouses',{data:{}});
}

const storeWarehouseApi = async (warehouse)=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.post('/api/warehouses',warehouse);
}

const updateWarehouseApi = async (warehouse)=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.put(`/api/warehouses/${warehouse.id}`,warehouse);
}

const deleteWarehouseApi = async (warehouse)=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.delete(`/api/warehouses/${warehouse.id}`,{data:{}});
}

const apiWarehouse = {
    getListWarehouseApi,
    storeWarehouseApi,
    updateWarehouseApi,
    deleteWarehouseApi
}

export {
    apiWarehouse
}