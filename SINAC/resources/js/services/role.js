import Axios from "axios";
import { getLocalUser } from '../helpers/auth';

let axios = Axios;
axios.defaults.headers.common['Content-Type'] = 'application/json';

const getRolesApi = async ()=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.get('/api/role_users/roles',{data:{}});
}

const apiRole = {
    getRolesApi,
}

export {
    apiRole
}