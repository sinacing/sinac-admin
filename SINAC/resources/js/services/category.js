import Axios from "axios";
import { getLocalUser } from '../helpers/auth';

let axios = Axios;
axios.defaults.headers.common['Content-Type'] = 'application/json';

const getListCategoryApi = async ()=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.get('/api/categories',{data:{}});
}

const storeCategoryApi = async (category)=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.post('/api/categories', category);
}

const updateCategoryApi = async (category)=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.put(`/api/categories/${category.id}`, category);
}

const deleteCategoryApi = async (category)=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.delete(`/api/categories/${category.id}`,{data:{}});
}

const apiCategory = {
    getListCategoryApi,
    storeCategoryApi,
    updateCategoryApi,
    deleteCategoryApi
}

export { apiCategory }