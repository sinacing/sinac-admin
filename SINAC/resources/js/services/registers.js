import Axios from "axios";
import { getLocalUser } from '../helpers/auth';

let axios = Axios;
axios.defaults.headers.common['Content-Type'] = 'application/json';

const getRegistersApi = async ()=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await Axios.get('/api/registers',{data:{}})
}

const storeRegisterApi = async register=>{
    return await Axios.post('/api/registers',register);
}

const acceptedRegisterApi = async register=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.post('/api/registers/acceptedRegister',{id:register.id})
}

const deleteRegisterApi = async register=>{
    console.log(register.id)
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.delete(`/api/registers/${register.id}`,{data:{}});
}


const apiRegister = {
    getRegistersApi,
    storeRegisterApi,
    acceptedRegisterApi,
    deleteRegisterApi
}

export {
    apiRegister
}