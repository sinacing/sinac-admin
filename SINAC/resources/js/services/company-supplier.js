import Axios from 'axios'
import { getLocalUser } from '../helpers/auth';

const getCompanySupplierApi = async ()=>{
    Axios.defaults.headers['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await Axios.get('/api/company-suppliers',{data:{}});
}

const storeCompanySupplierApi = async company=>{
    Axios.defaults.headers['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await Axios.post('/api/company-suppliers', company);
}

const updateCompanySupplierApi = async company=>{
    Axios.defaults.headers['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await Axios.put(`/api/company-suppliers/${company.id}`, company);
}

const disableCompanySupplierApi = async data=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.post('/api/company-suppliers/disable',data)
}

const apiCompanySupplier = {
    getCompanySupplierApi,
    storeCompanySupplierApi,
    updateCompanySupplierApi,
    disableCompanySupplierApi

}

export { apiCompanySupplier }