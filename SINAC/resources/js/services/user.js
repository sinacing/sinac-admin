import Axios from "axios";
import { getLocalUser } from '../helpers/auth';

let axios = Axios;
axios.defaults.headers.common['Content-Type'] = 'application/json';

const getListUserApi = async () =>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.get('/api/users',{data:{}})
}

const storeUserApi = async data=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.post('/api/users', data)
}

const updateUserApi = async userData=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.put(`/api/users/${userData.id}`,userData)
}

const disableUserApi = async data=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.post(`/api/users/disable`,data)
}

const filterUserApi = async valueFilter =>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.post('/api/users/filterUser',{valueFilter});
}

const deleteUserApi = async userData =>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.delete(`/api/users/${userData.id}`,{data:{}})
     
} 

const apiUser = {
    getListUserApi,
    storeUserApi,
    updateUserApi,
    disableUserApi,
    filterUserApi,
    deleteUserApi
}

export {
    apiUser
}