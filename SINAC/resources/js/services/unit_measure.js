import Axios from 'axios'
import { getLocalUser } from '../helpers/auth';

let axios = Axios;
axios.defaults.headers.common['Content-Type'] = 'application/json';

const getUnitMeasureApi = async ()=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.get('/api/unitmeasure',{data:{}})   
}

const apiUnitMeasure = {
    getUnitMeasureApi
}

export {
    apiUnitMeasure,  
}