import Axios from "axios";
import { getLocalUser } from '../helpers/auth';

let axios = Axios;
axios.defaults.headers.common['Content-Type'] = 'application/json';

const getRoleUserListApi = async ()=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.get('/api/role-users',{data:{}}); // cambiar esas rutas
}

// const destroyRoleUserApi = async id =>{
//     axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
//     return await axios.delete(`/api/role_users/${id}`,{data:{}});
// }

// const storeRoleUserApi = async data =>{
//     axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
//     return await axios.post('/api/role_users',data);
// }

const modifyRoleUserApi = async data=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.put(`/api/role-users/${data.user.id}`,{roles:data.roles});
}

const apiRoleUser = {
    getRoleUserListApi,
    // destroyRoleUserApi,
    // storeRoleUserApi,
    modifyRoleUserApi
}

export {
    apiRoleUser
}