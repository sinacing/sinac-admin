import Axios from 'axios'
import { getLocalUser } from '../helpers/auth';

let axios = Axios;
axios.defaults.headers.common['Content-Type'] = 'application/json';

const getTypeCommodityApi = async ()=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.get('/api/typecommodity',{data:{}});
}


const apiTypeCommodity = {
    getTypeCommodityApi,
    
}

export {
    apiTypeCommodity,  
}