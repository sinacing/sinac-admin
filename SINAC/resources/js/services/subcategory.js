import Axios from "axios";
import { getLocalUser } from '../helpers/auth';

let axios = Axios;
axios.defaults.headers.common['Content-Type'] = 'application/json';

const getListSubCategoryApi = async ()=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.get('/api/subcategories',{data:{}})
}

const storeSubCategoryApi = async (subcategory)=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.post('/api/subcategories',subcategory);
}

const updateSubCategoryApi = async (subcategory)=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.put(`/api/subcategories/${subcategory.id}`,subcategory)
}

const deleteSubCategory = async (subcategory)=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.delete(`/api/subcategories/${subcategory.id}`,{data:{}});
}

const apiSubCategory = {
    getListSubCategoryApi,
    storeSubCategoryApi,
    updateSubCategoryApi,
    deleteSubCategory
}

export {
    apiSubCategory
}