import Axios from 'axios'
import { getLocalUser } from '../helpers/auth';

let axios = Axios;
axios.defaults.headers.common['Content-Type'] = 'application/json';

const getListActiveApi = async () =>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.get('/api/actives',{data:{}})
}

const storeActiveApi = async active => {
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.post('/api/actives',active)
}

const updateActiveApi = async active=> {
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.put(`/api/actives/${active.id}`,active)
}

const deleteActiveApi = async active => {
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    axios.delete(`/api/actives/${active.id}`,{data:{}})
}

const disableActiveApi = async data=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.post(`/api/actives/disable`,data)
}
const filterActiveApi = async valueFilter=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.post('/api/actives/filterActive',{valueFilter});
}
const apiActives = {
    getListActiveApi,
    storeActiveApi,
    updateActiveApi,
    deleteActiveApi,
    disableActiveApi,
    filterActiveApi
}

export {
    apiActives,  
}
/*TODO simple promesa
 *  const deleteActiveApi = (activeData) => new Promise((res,rej)=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    axios.delete(`/api/actives/${activeData.id}`,{data:{}})
     .then(response => res(response))
     .catch(err=>rej(err.response))
}) 
 */