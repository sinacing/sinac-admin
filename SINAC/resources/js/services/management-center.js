import Axios from 'axios';
import { getLocalUser } from '../helpers/auth';

let axios = Axios;
axios.defaults.headers.common['content-type'] = 'application/json';

const getListManagement_centerApi = async()=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.get('/api/management-centers',{data:{}});
}
const storeManagement_centerApi = async(managementCenter)=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.post('/api/management-centers',managementCenter);
}
const updateManagement_centerApi = async(managementCenter)=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.put(`/api/management-centers/${managementCenter.id}`,managementCenter);
}
const deleteManagement_centerApi = async (managementCenter)=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.delete(`/api/management-centers/${managementCenter.id}`,{data:{}});
}

const apiManagementCenter ={
    getListManagement_centerApi,
    storeManagement_centerApi,
    updateManagement_centerApi,
    deleteManagement_centerApi
}
export{
    apiManagementCenter
}