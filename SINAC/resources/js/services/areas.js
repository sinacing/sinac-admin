import Axios from "axios";
import { getLocalUser } from '../helpers/auth';

let axios = Axios;
axios.defaults.headers.common['Content-Type'] = 'application/json';

const getListAreaApi = () => new Promise((res,rej)=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    axios.get('/api/areas',{data:{}})
    .then(response=> res(response))
    .catch(err => rej(err.response))
})
const updateAreaApi = async area=> {
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.put(`/api/areas/${area.id}`,area);
}

const storeAreaApi = async area => {
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.post('/api/areas',area)
}

const deleteAreaApi = async area=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.delete(`/api/areas/${area.id}`,{data:{}})
}

const apiArea = {
    getListAreaApi,
    updateAreaApi,
    storeAreaApi,
    deleteAreaApi
}

export {
    apiArea
}