import Axios from "axios";
import { getLocalUser } from '../helpers/auth';

let axios = Axios;
axios.defaults.headers.common['Content-Type'] = 'application/json';

const getPersonSupplierApi = async ()=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.get('/api/person-suppliers', {data:{}});
}

const storePersonSupplierApi = async person=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.post('/api/person-suppliers',person);
}

const updatePersonSupplierApi = async person=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.put(`/api/person-suppliers/${person.id}`,person);
}

const disablePersonSupplierApi = async data=>{
    console.log(data)
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.post('/api/person-suppliers/disable',data)
}

const apiPersonSupplier = {
    getPersonSupplierApi,
    storePersonSupplierApi,
    updatePersonSupplierApi,
    disablePersonSupplierApi
    
}

export {
    apiPersonSupplier
}