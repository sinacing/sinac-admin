import Axios from "axios";

const login = (credentials)=> new Promise((res,rej)=>{
    
    Axios.post('/api/auth/login',credentials)
        .then(response=> res(response.data))
        .catch(err=> rej('Usuario o contraseña son invalidos!'))
})

const logout = async ()=>{
   const loggedOut = await Axios.post('/api/auth/logout')
   return loggedOut.data;
}

const apiAuthenticate = {
    login,
    logout
}

export {
    apiAuthenticate
}