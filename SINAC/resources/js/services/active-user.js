import Axios from 'axios'
import { getLocalUser } from '../helpers/auth';

let axios = Axios;
axios.defaults.headers.common['Content-Type'] = 'application/json';

const getListActiveAsignedApi = async () =>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.get('/api/active-users',{data:{}})
}

const storeActiveAsignedApi = async (credentials) => {
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.post('/api/active-users',credentials)
}

const deallocateActiveApi = async id=>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${getLocalUser().token}`;
    return await axios.delete(`/api/active-users/${id}`,{data:{}})
}

const apiActiveUser = {
    getListActiveAsignedApi,
    storeActiveAsignedApi,
    deallocateActiveApi
}

export {
    apiActiveUser,  
}