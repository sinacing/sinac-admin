import Vue from 'vue'
import VueSpinner from 'vue-spinners'

Vue.use(VueSpinner)

export default VueSpinner;