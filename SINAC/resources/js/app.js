import Vue from 'vue';
import vuetify from './plugins/vuetify'
import App from './views/App.vue'
import router from './routes/router'
import store from './store'

new Vue({
    vuetify,
    router,
    store,
    render: page=> page(App)
}).$mount('#app') 
