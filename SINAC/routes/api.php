<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'],function () {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});

Route::group(['prefix' => 'registers','middleware' => ['cors']], function () {
    Route::get('/', 'RegisterController@index');
    Route::post('/','RegisterController@store');
    Route::post('/acceptedRegister','RegisterController@acceptedRegister');
    Route::delete('/{register}','RegisterController@destroy');
});

Route::group(['prefix' => 'users', 'middleware'=>['cors','jwt.auth']], function () {
    Route::get('/','UserController@index');
    Route::post('/','UserController@store');
    Route::put('/{user}','UserController@update');
    Route::delete('/{user}','UserController@destroy');
    Route::post('/disable','UserController@disable');
    Route::post('/filterUser','UserController@filterUser');
});

Route::group(['prefix' => 'role-users' ,'middleware' => ['cors','jwt.auth']], function () {
    Route::get('/','RoleUserController@index');
    Route::put('/{role_user}','RoleUserController@modify');
    Route::post('/','RoleUserController@store');
    // roles
    Route::get('/roles','RoleUserController@roles');
});

// ACTIVES
Route::group(['prefix'=>'actives', 'middleware' => ['cors','jwt.auth']], function () {
    Route::get('/','ActiveController@index');
    Route::post('/','ActiveController@store');
    Route::put('/{active}','ActiveController@update');
    Route::delete('/{active}','ActiveController@destroy');
    Route::post('/disable','ActiveController@disable');
    Route::post('/filterActive','ActiveController@filterActive');
});

Route::group(['prefix'=>'active-users', 'middleware' => ['cors','jwt.auth']], function () {
    Route::get('/','ActiveUserController@index');
    Route::post('/','ActiveUserController@store');
    Route::delete('/{active_user}','ActiveUserController@destroy');
});

Route::group(['prefix'=>'status', 'middleware' => ['cors','jwt.auth']], function () {
    Route::get('/','StatusController@index');
    
});
Route::group(['prefix'=>'quantity', 'middleware' => ['cors','jwt.auth']], function () {
    Route::get('/','QuantityController@index');
});

Route::group(['prefix'=>'areas', 'middleware' => ['cors','jwt.auth']], function () {
    Route::get('/','AreaController@index');
    Route::post('/','AreaController@store');
    Route::put('/{area}','AreaController@update');
    Route::delete('/{area}','AreaController@destroy');
});

Route::group(['prefix'=>'commodities', 'middleware' => ['cors','jwt.auth']], function () {
    Route::get('/','CommodityController@index');
    Route::post('/','CommodityController@store');
    Route::put('/{commodity}','CommodityController@update');
    Route::post('/disable','CommodityController@disable');
    Route::post('/filterCommodity','CommodityController@filterCommodity');
});

Route::group(['prefix'=>'categories', 'middleware' => ['cors','jwt.auth']], function () {
    Route::get('/','CategoryController@index');
    Route::post('/','CategoryController@store');
    Route::put('/{category}','CategoryController@update');
    Route::delete('/{category}','CategoryController@destroy');
    
});

Route::group(['prefix'=>'subcategories', 'middleware' => ['cors','jwt.auth']], function () {
    Route::get('/','SubCategoryController@index');
    Route::post('/','SubCategoryController@store');
    Route::put('/{subcategory}','SubCategoryController@update');
    Route::delete('/{subcategory}','SubCategoryController@destroy');
});

Route::group(['prefix'=>'warehouses', 'middleware' => ['cors','jwt.auth']], function() {
    Route::get('/','WarehouseController@index');
    Route::post('/','WarehouseController@store');
    Route::put('/{warehouse}','WarehouseController@update');
    Route::delete('/{warehouse}','WarehouseController@destroy');
});
Route::group(['prefix'=>'sales', 'middleware' => ['cors','jwt.auth']], function() {
    Route::get('/','SaleController@index');
    Route::post('/','SaleController@store');
    Route::put('/{sale}','SaleController@update');
    // Route::delete('/{sale}','SaleController@destroy');
});

Route::group(['prefix'=>'typecommodity','middleware' => ['cors','jwt.auth']], function () {
    Route::get('/','TypeCommodityController@index');
});

Route::group(['prefix'=>'unitmeasure','middleware' => ['cors','jwt.auth']], function () {
    Route::get('/','UnitMeasureController@index');
});

Route::group(['prefix'=>'person-suppliers','middleware'=>['cors','jwt.auth']],function(){
    Route::get('/','PersonSupplierController@index');
    Route::post('/','PersonSupplierController@store');
    Route::post('/disable','PersonSupplierController@disable');
    Route::put('/{person_supplier}','PersonSupplierController@update');
});

Route::group(['prefix'=>'company-suppliers','middleware'=>['cors','jwt.auth']],function(){
    Route::get('/','CompanySupplierController@index');
    Route::post('/','CompanySupplierController@store');
    Route::post('/disable','CompanySupplierController@disable');
    Route::put('/{company_supplier}','CompanySupplierController@update');
});

Route::group(['prefix'=>'management-centers','middleware'=>['cors','jwt.auth']], function (){
    Route::get('/', 'ManagementCenterController@index');
    Route::post('/', 'ManagementCenterController@store');
    Route::put('/{management_center}','ManagementCenterController@update');
});
