<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonSupplier extends Model
{
   protected $fillable = ['dni','first_name','middle_name','first_surname','second_surname','telephone','direction','bank_account','status_id'];

   public function getAllPersonSupplier(){
      $personSupplier = $this->all();
      foreach($personSupplier as $key => $value):
         $personSupplier[$key]->status_id = Status::find($value->status_id,['id','type']);
      endforeach;

      return $personSupplier;
   }

   public function getPersonSupplier($personSupplier){
      $personSupplier->status_id = Status::find($personSupplier->status_id,['id','type']);
      return $personSupplier;
   }
}
