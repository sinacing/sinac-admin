<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const admin = 1;
    const administratorEmployee = 2;

    protected $fillable = ['type','description'];
}
