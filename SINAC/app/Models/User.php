<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Status;
use App\Active;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','middle_name','first_surname','second_surname','email','dni','telephone','direction','image','status_id','area_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token','password'
    ];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    
    public function userList()
    {
        $users = $this->all();

        foreach($users as $key => $value):
            // $users[$key]['password'] = $this->decryptPassword($value); // desencriptamos la contraseña
            $users[$key]['status_id'] = $value->status;
            unset($users[$key]['status']);
        endforeach;

        return $users;
    }
    
    public function getUser($user){
        $userAtributtes = User::all()->where('dni',$user['dni'])->first();
        $userAtributtes->status_id = $userAtributtes->status;
        unset($userAtributtes->status); //sacamos el status debido a que si actualizamos se ira 
        return $userAtributtes;
    }

    public function getUserActive() //usuarios activos
    {
        $activeUser = DB::table('users')->where('status_id', Status::active)->get();
        return $this->getFilterUser($activeUser);
    }

    public function getUserDesactive() //usuarios inactivos
    {
        $noActiveUser = DB::table('users')->where('status_id', Status::no_active)->get();
        return $this->getFilterUser($noActiveUser);
    }

    public function getFilterUser($data){
        foreach($data as $key => $value):
            $data[$key]->status_id = Status::find($value->status_id,['id','type']);
        endforeach;

        return $data;
    }

    public function administratorEmployee(){

        $employees = DB::table('users')
        ->join('role_users','users.id','=','role_users.user_id')
        ->join('roles','roles.id','=','role_users.role_id')
        ->select('users.id','users.first_name','users.first_surname','users.second_surname','users.dni','users.telephone','roles.type')
        ->where('role_users.role_id',2)
        ->get();

        foreach($employees as $key =>$value):
            $roles = DB::table('roles')
            ->join('role_users','roles.id','=','role_users.role_id')
            ->where('role_users.user_id',$value->id)
            ->select('roles.id','roles.type')
            ->get(); 
            $employees[$key]->type = $roles; 
        endforeach;

        return $employees;
    }
    
    public function decryptPassword($user){
        return decrypt($user->password);
    }
    

    public function setPasswordAttribute($password) // cada vez que se vea involucrado el campo password
    {
        if($password !=null):
            $this->attributes['password'] = bcrypt($password);
        endif;
    }


    /** Relations ship */
    public function status()
    {
        return $this->belongsTo(Status::class)->select('id','type');
    }

    public function active()
    {
        return $this->hasMany(Active::class);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
