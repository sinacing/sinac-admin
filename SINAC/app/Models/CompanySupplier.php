<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanySupplier extends Model
{
    protected $fillable = ['dni','name','telephone','direction','bank_account','status_id'];

    public function getAllCompanySupplier(){
        $companySupplier = $this->all();
        foreach($companySupplier as $key => $value):
           $companySupplier[$key]->status_id = Status::find($value->status_id,['id','type']);
        endforeach;
  
        return $companySupplier;
     }

    public function getCompanySupplier($companySupplier){
        $companySupplier->status_id = Status::find($companySupplier->status_id,['id','type']);
        return $companySupplier;
    }
}
