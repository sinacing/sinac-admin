<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Status extends Model
{
    
    const active = 1;
    const no_active = 2;
    const good = 3;
    const regular = 4;
    const bad = 5;

    public function statuses(){
        return Status::all();
    }
    
    /** Relations ship */
    public function user()
    {
        return $this->hasMany(User::class);
    }
}
