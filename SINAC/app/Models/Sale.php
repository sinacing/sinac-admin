<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Area;
use App\User;
use Illuminate\Support\Facades\DB;

class Sale extends Model
{
    protected $fillable =['id','saller_user_id','buyer_user_id','area_id','created_at'];

    public function salesList()
    {

        $sale = $this->all();

        foreach ($sale as $key => $value):

            $sale[$key]['area_id'] = $value->area;
            unset($sale[$key]['user_buyer'],$sale[$key]['area']);
       
        endforeach;
        return $sale;
    }

    public function getSale($sale)
    {
       $attrSale = $this->all()->where('id',$sale['id'])->first();

       $attrSale->area_id = $attrSale->area;
       unset($attrSale->buyer_user,$attrSale->area);
       return $attrSale;
    }

    public function getDatesSale($dates){
        return DB::table('sales')->whereBetween('created_at',[$dates[0],$dates[1]])->get();
    }

    public function area()
    {
       return $this->belongsTo(Area::class)->select('name');
    }
    public function user()
    {
        return $this->belongsTo(User::class)->select('first_name');
    }
}
