<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManagementCenter extends Model
{
    protected $fillable = ['name', 'code'];
    public function getManagementCenter($managementCenter){
        return $this->all()->where('code',$managementCenter['code'])->first();
    }
}
