<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;

class Subcategory extends Model
{
    protected $fillable = ['name','code','description','category_id'];
    
    public function getSubcategories(){
        $categories = $this->all();
        foreach($categories as $key => $value):
            $categories[$key]['category_id'] = $value->category;
            unset($categories[$key]['category']);
        endforeach;

        return $categories;
    }

    public function getSubcategory($subcategory){
        $subcategory = $this->all()->where('code',$subcategory['code'])->first();
        $subcategory->category_id = $subcategory->category;
        unset($subcategory->category);
        return $subcategory;
    }

    public function category(){
        return $this->belongsTo(Category::class)->select('id','name');
    }
}
