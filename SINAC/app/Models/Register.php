<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
    protected $fillable = ['first_name','middle_name','first_surname','second_surname','email','dni', 'password','telephone','direction','area_id','role_id'];

    function getRegisters()
    {
        $registers = $this->all();
        foreach($registers as $key => $value):
            $registers[$key]['role_id'] = $this->role($value['role_id']);
            $registers[$key]['area_id'] = $this->area($value['area_id']);
        endforeach;
        
        return $registers;
    }

    function role($id)
    {
        return Role::find($id,['id','type']);
    }

    function area($id){
        return Area::find($id,['id','name']);
    }
}
