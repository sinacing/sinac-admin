<?php

namespace App;

class HandleMessage {
    
    const unAuth = 'Unauthorized';

    static function activeMsg($status){
        return "Activo ". self::statusMessage($status) ." correctamente";
    }

    static function errActMsg($status){
        return ['message'=> "Error al ". self::statusMessage($status). " el activo"];
    }

    static function areaMsg($status){
        return "Departamento ". self::statusMessage($status)." correctamente";
    }

    static function errAreaMsg($status){
        return ['message'=> "Error al ". self::statusMessage($status). " el departamento"];
    }

    static function categoryMsg($status){
        return "Partida ". self::statusMessage($status) ." correctamente";
    }

    static function errCategoryMsg($status){
        return ['message'=> "Error al ". self::statusMessage($status) . "la partida"];
    }

    static function managCenterMsg($status){
        return "Centro gestor ". self::statusMessage($status) ." correctamente";
    }

    static function errManagCenterMsg($status){
        return ['message'=> "Error al ". self::statusMessage($status) . "el centro gestor"];
    }

    static function supplMsg($status){
        return "Proveedor ". self::statusMessage($status) ." correctamente";
    }

    static function errSupplMsg($status){
        return ['message'=> "Error al ". self::statusMessage($status) . "el proveedor"];
    }

    static function statusMessage($status){
        $msg = ['ingresado','ingresar','modificado','modificar','eliminado','eliminar','asignado','asignar',
        'desabilitado','desabilitar','desasignado','desasignar'];
        return $msg[$status - 1];
    }

    
}