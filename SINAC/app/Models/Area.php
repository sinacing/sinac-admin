<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Active;

class Area extends Model
{
    const defaultArea = 1; //Administracion

    protected $fillable = ['name','location','telephone','description'];   
    
    public function actives()
    {
        return $this->hasMany(Active::class);
    }

    public function getArea($area, $id){
        $area['id'] = $id;
        return $area;
    }
}
