<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use App\Status;
use App\Subcategory;
use App\Warehouse;

use Illuminate\Support\Facades\DB;

class Active extends Model
{
    use Notifiable;
    
    protected $fillable = [
        'code','description','brand','model','value','user_id','area_id','dependency','status_id', 'subcategory_id', 'warehouse_id'
    ];

    public function getActives(){
        $actives = $this->all();
        foreach($actives as $key => $value):
            $actives[$key]['status_id'] = Status::find($value->status_id,['id','type']);
            $actives[$key]['subcategory_id'] = Subcategory::find($value->subcategory_id,['id','name']);
            $actives[$key]['warehouse_id'] = Warehouse::find($value->warehouse_id,['id','name']);
        endforeach;

        return $actives;
    }

    /*public function activeList()
    {
        $activeList = [];
        $active = $this->all();
        dd($active);
        $active_user = ActiveUser::all();

        $activeId = $this->extractActiveId($active);
        $activeUserId = $this->extractActiveUserId($active_user);

        foreach($activeId as $key => $value_active):
            if(!in_array($value_active, $activeUserId)):
                $active[$key]['status_id'] =  Status::find($active[$key]['status_id'],['id','type']);
                array_push($activeList,$active[$key]);
            endif;
        endforeach;

        return $activeList;
    }*/

    public function getActive($active){
        $attrActive = $this->all()->where('code',$active['code'])->first();
        
        $attrActive->status_id = $attrActive->status;
        unset($attrActive->status);
        return $attrActive;
    }
    private function getActiveGood(){
        $active = DB::table('actives')->where('status_id',Status::good)->get(); 
        return $this->getFilterActive($active);
    }

    private function getActiveRegular(){
        $active =  DB::table('actives')->where('status_id',Status::regular)->get();
        return $this->getFilterActive($active);
    }

    private function getActiveBad(){
        $active = DB::table('actives')->where('status_id',Status::bad)->get();
        return $this->getFilterActive($active);
    }

    public function filterStatusActive($valueFilter){
        switch ($valueFilter) {
            case 1:
                return response()->json(['actives'=>$this->getActives()]);
                break;
            case 2:
                return response()->json(['actives'=>$this->getActiveGood()]);
                break;
            case 3:
                return response()->json(['actives'=>$this->getActiveRegular()]);
                break;
            case 4:
                return response()->json(['actives'=>$this->getActiveBad()]);
                break;
            default:
                return response()->json(['actives'=>$this->getActives()]);
                break;
        }
    }

    public function getFilterActive($data){
        foreach($data as $key => $value):
            $data[$key]->status_id = Status::find($value->status_id,['id','type']);
        endforeach;

        return $data;
    }

    function extractActiveId($actives)
    {
        $activeId = [];
        foreach($actives as $value):
            array_push($activeId,$value->id);
        endforeach;
        return $activeId;
    }

    function extractActiveUserId($activeUser){
        $activeUserId = [];
        foreach($activeUser as $value):
            array_push($activeUserId,$value->active_id);
        endforeach;
        return $activeUserId;
    }

    /** Relations ship */
    public function status()
    {
        return $this->belongsTo(Status::class)->select('id','type');
    }

}
