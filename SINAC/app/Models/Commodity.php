<?php

namespace App;
use Iluminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use App\Status;
use App\Subcategory;
use App\Warehouse;
use App\Quantity;
use Illuminate\Support\Facades\DB;

class Commodity extends Model
{
     //use notifiable;
     protected $fillable = [
       'code','name','price_unit','price_total','subcategory_id','status_id','warehouse_id','quantity_id','created_at'
     ];
     
     public function commodityList()
     {
        $commodity = $this->all();
        
        foreach($commodity as $key => $value):
           
            $commodity[$key]['subcategory_id'] = $value->subcategory;
            $commodity[$key]['status_id'] = $value->status;
            $commodity[$key]['warehouse_id'] = $value->warehouse;
            $commodity[$key]['quantity_id'] = $value->quantity;
            unset($commodity[$key]['subcategory'], $commodity[$key]['status'], $commodity[$key]['warehouse'], $commodity[$key]['quantity']); //sacamos el status debido a que si actualizamos se ira 
        endforeach;
        
        return $commodity;
     }
     
    public function getCommodity($commodity){
        $attrCommodity = $this->all()->where('code',$commodity['code'])->first();
        
        $attrCommodity->subcategory_id= $attrCommodity->subcategory;
        $attrCommodity->warehouse_id = $attrCommodity->warehouse;
        $attrCommodity->status_id = $attrCommodity->status;
        $attrCommodity->quantity_id = $attrCommodity->quantity;
        unset($attrCommodity->subcategory,$attrCommodity->warehouse,$attrCommodity->status,$attrCommodity->quantity);
        
        return $attrCommodity;
    }

    public function getCommodityActive(){
        $activeCommodity = DB::table('commodities')->where('status_id',Status::active)->get(); 
        return $this->getFilterCommodity($activeCommodity);
   }

    public function getCommodityDesactive(){
        $noActive =  DB::table('commodities')->where('status_id',Status::no_active)->get();
        return $this->getFilterCommodity($noActive);
    }

    public function getCommodityLowStock(){
        $lowStock = [];
        $commodity = $this->commodityList();

        foreach($commodity as $value):
            if($value->quantity_id->stock <= 300):
                array_push($lowStock,$value);
            endif;
        endforeach;

        return $lowStock;
    }

    public function getFilterCommodity($data){

        foreach($data as $key => $value):
            $data[$key]->subcategory_id = Subcategory::find($value->subcategory_id,['id','name']);
            $data[$key]->warehouse_id = Warehouse::find($value->warehouse_id,['id','name']);
            $data[$key]->status_id = Status::find($value->status_id,['id','type']);
            $data[$key]->quantity_id = Quantity::find($value->quantity_id,['id','stock','quanty','total_quanty','unit_measure_id','type_commodity_id']);
        endforeach;

        return $data;
    }

    /** Relations ship */
    public function subcategory()
    {
        return $this->belongsTo(Subcategory::class)->select('id','name');
    }
    
    public function status()
    {
        return $this->belongsTo(Status::class)->select('id','type');
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class)->select('id','name');
    }
    public function quantity()
    {
        return $this->belongsTo(Quantity::class)->select('id','stock','quanty','total_quanty','unit_measure_id','type_commodity_id');
    }
}
