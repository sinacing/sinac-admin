<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Subcategory;

class Category extends Model
{
    protected $fillable = [
        'name','code','description'
    ];

    public function getCategory($category, $id){
        $category['id'] = $id;
        return $category;
    }

    public function subcategories(){
        return $this->hasMany(Subcategory::class);
    }
}
