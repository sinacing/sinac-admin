<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    protected $fillable = ['name','code','description'];

    public function getWarehouse($warehouse){
        return $this->all()->where('code',$warehouse['code'])->first();
    }
}
