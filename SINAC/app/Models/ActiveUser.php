<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActiveUser extends Model
{
    protected $fillable = ['id','user_id','active_id','created_at','updated_at'];
    
    public function activeAsignedList()
     {
        $activeAsigned = $this->all();
        foreach($activeAsigned as $key => $value):
            $activeAsigned[$key]['user_id'] = $this->getUser($value->user_id);
            $activeAsigned[$key]['active_id'] = $this->getActive($value->active_id);
        endforeach;
        
        return $activeAsigned;
     }
     
    public function getActiveAsigned($activeAsigned){
        $active_user['user_id'] = $this->getUser($activeAsigned->user_id);
        $active_user['active_id'] = $this->getActive($activeAsigned->active_id);

        return $active_user;
    }

    public function getUser($user_id){
        return User::find($user_id,['id','dni','first_name','first_surname','second_surname']);
    }

    public function getActive($active_id){
        return Active::find($active_id,['id','code','model','brand','description']);
    }
}
