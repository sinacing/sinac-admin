<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SaleDetail extends Model
{
    public function getDatesSaleDetail($dates){
        return DB::table('sale_details')->whereBetween('created_at',[$dates[0],$dates[1]])->get();
    }
}
