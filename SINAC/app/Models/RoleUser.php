<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RoleUser extends Model
{
    protected $fillable = ['user_id','role_id'];

    function getRolesUser($id){
        $employee = DB::table('users')
        ->join('role_users','users.id','=','role_users.user_id')
        ->join('roles','roles.id','=','role_users.role_id')
        ->select('users.id','users.first_name','users.first_surname','users.second_surname','users.dni','users.telephone','roles.type')
        ->where('role_users.role_id',2)
        ->where('role_users.user_id',$id)
        ->get();

        $roles = DB::table('roles')
        ->join('role_users','roles.id','=','role_users.role_id')
        ->where('role_users.user_id',$id)
        ->select('roles.id','roles.type')
        ->get(); 

        $employee[0]->type = $roles;

        return $employee;
    }
}
