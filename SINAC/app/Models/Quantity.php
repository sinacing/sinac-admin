<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quantity extends Model
{
    protected $fillable = [
        'stock','quanty','total_quanty','unit_measure_id','type_commidity_id' 
    ];

    public function __construct($stock = 0,$quanty = 0,$total_quanty = 0,$unit_measure_id = 0, $type_commodity_id = 0)
    {
        $this->stock = $stock;
        $this->quanty = $quanty;
        $this->total_quanty = $total_quanty;
        $this->unit_measure_id = $unit_measure_id;
        $this->type_commodity_id = $type_commodity_id;
    }

    public function quantityList()
     {
        $quantity = $this->all();

        foreach($quantity as $key => $value):
            $quantity[$key]['unit_measure_id'] = $value->unit_measure;
            $quantity[$key]['type_commidity_id'] = $value->type_commodity;
            unset($quantity[$key]['unit_measure'], $quantity[$key]['type_commodity']); //sacamos el status debido a que si actualizamos se ira 
        endforeach;
        
        return $quantity;

     }
}
