<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ManagementCenterRequest;
use App\HandleMessage;
use App\ManagementCenter;
use Illuminate\Support\Facades\Validator;
use PDOException;
use Exception;

class ManagementCenterController extends Controller
{
    function __construct()
    {
        $this->managementCenter = new ManagementCenter();
        $this->managementCenterValidator = new ManagementCenterRequest();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       if(request()->isJson()):
            return response()->json(['management_center'=>ManagementCenter::all()],200);
        endif;
        return response()->json(HandleMessage::unAuth,401);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->isJson()):
            return response()->json(HandleMessage::unAuth,401);
        endif;

        try{
            $managementCenter = $request->all();
            $validator = Validator::make($managementCenter, $this->managementCenterValidator->rules(), $this->managementCenterValidator->messages());

            if($validator->fails()):
                return response()->json(["errors" => $validator->errors()],400);
            endif;

            if($result = ManagementCenter::create($managementCenter)):
                return response()->json(['management_center'=>$result, 'message'=>HandleMessage::managCenterMsg(1)],201);
            endif;
            return response()->json(HandleMessage::errManagCenterMsg(2),500);
        }
        catch(PDOException | Exception $e){
            return response()->json(['errors'=>$e->getMessage()],500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ManagementCenter $managementCenter)
    {
        if(!$request->isJson()):
            return response()->json(HandleMessage::unAuth,401);
        endif;

        try{
            $data = $request->all();
            $validator = Validator::make($data, $this->managementCenterValidator->rules($managementCenter,true), $this->managementCenterValidator->messages());
        
            if($validator->fails()):
                return response()->json(["errors" => $validator->errors()],400);
            endif;

            if($result = tap($managementCenter)->update($data)):
                return response()->json(['management_center' => $result ,'message' => HandleMessage::managCenterMsg(3)],200);
            endif;
            return response()->json(HandleMessage::errManagCenterMsg(4),500);
        }
        catch(PDOException | Exception $e){
            return response()->json(['errors'=>$e->getMessage()],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
