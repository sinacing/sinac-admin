<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TypeCommodity;
use Exception;

class TypeCommodityController extends Controller
{
    public function index(){
        if(!request()->isJson()):
            return response()->json("Unauthorized",401);
        endif;

        try{
            return response()->json(['typecommodities'=>TypeCommodity::all()]);
        }
        catch (\PDOException | Exception $e){
            return response()->json(['errors'=>$e->getMessage()],500);
        }
             
    }
}
