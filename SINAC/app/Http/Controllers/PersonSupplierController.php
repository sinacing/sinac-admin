<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PersonSupplier;
use App\HandleMessage;
use App\Http\Requests\PersonSupplierRequest;
use App\Status;
use PDOException;
use Exception;
use Illuminate\Support\Facades\Validator;



class PersonSupplierController extends Controller
{
    public function __construct()
    {
        $this->personSupplier = new PersonSupplier();
        $this->personSupplierValidator = new PersonSupplierRequest();    
    }


    public function index()
    {
        return request()->isJson() ? response()->json(['person_supplier'=>$this->personSupplier->getAllPersonSupplier()]) :
                                    response()->json(HandleMessage::unAuth,401); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->isJson()):
            return response()->json(HandleMessage::unAuth,401);
        endif;

        try{
            $person = $request->all();
            $validator = Validator::make($person, $this->personSupplierValidator->rules(), $this->personSupplierValidator->messages());
            if($validator->fails()):
                return response()->json(["errors" => $validator->errors()],400);
            endif;

            if($result = PersonSupplier::create($person)):
                return response()->json(['person_supplier'=> $this->personSupplier->getPersonSupplier($result), 'message'=>HandleMessage::supplMsg(1)],201);
            endif;
            return response()->json(HandleMessage::errSupplMsg(2),500);

        }catch(PDOException | Exception $e){
            return response()->json(['errors'=>$e->getMessage()],500);
        } 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PersonSupplier $personSupplier)
    {
        if(!$request->isJson()):
            response()->json(HandleMessage::unAuth,401);
        endif;

        try{

            $data = $request->all();
            $data['status_id'] = is_array($data['status_id']) ? $data['status_id']['id'] : $data['status_id']; 
            $validator = Validator::make($data,$this->personSupplierValidator->rules($personSupplier,true), $this->personSupplierValidator->messages());
            if($validator->fails()):
                return response()->json(["errors" => $validator->errors()],400);
            endif;

            if($result = tap($personSupplier)->update($data)):
                return response()->json(['person_supplier'=> $this->personSupplier->getPersonSupplier($result),'message'=>HandleMessage::supplMsg(3)],200);
            endif;
            return response()->json(HandleMessage::errSupplMsg(4),500);

        }catch(PDOException | Exception $e){
            return response()->json(['errors'=>$e->getMessage()],500);
        }
    }

    public function disable(Request $request){
        if(!$request->isJson()):
            response()->json(HandleMessage::unAuth,401);
        endif;

        try{
            $data = $request->all();
            $personSupplier = PersonSupplier::find($data['id']);
            // here send only 1 = true, 0 = false
            $personSupplier->status_id = $data['status_id'] ? Status::active : Status::no_active;

            if($personSupplier->save()):
               return response()->json(['person_supplier'=> $this->personSupplier->getPersonSupplier($personSupplier)],200);
            endif;
            return response()->json(HandleMessage::errSupplMsg(4),500);

        }catch(PDOException | Exception $e){
            return response()->json(['errors'=>$e->getMessage()],500);
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
