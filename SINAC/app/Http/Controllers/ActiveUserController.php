<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ActiveUser;
use Exception;
use App\HandleMessage;
use App\Http\Requests\ActiveUserRequest;
use Illuminate\Support\Facades\Validator;


class ActiveUserController extends Controller
{
    public function __construct()
    {
        $this->active_users = new ActiveUser();
        $this->active_user_validate = new ActiveUserRequest();
    }

    public function index()
    {
        if(request()->isJson()):
            return response()->json(['activesAsigned'=> $this->active_users->activeAsignedList()]);
        endif;
        return response()->json(HandleMessage::unAuth,401);
    }
    
    public function store(Request $request)
    {        
        if(!$request->isJson()):
            return response()->json(HandleMessage::unAuth,401);
        endif;

        try{
            
            $ActiveAsigned = $request->all(); 
            $validator = Validator::make($ActiveAsigned,$this->active_user_validate->rules(),$this->active_user_validate->messages());
            if($validator->fails()):
                return response()->json(["errors" => $validator->errors()],400);
            endif;
            if($activeUserResult = ActiveUser::create($ActiveAsigned)):
                return response()->json(['activeAsigned'=> $this->active_users->getActiveAsigned($activeUserResult) ,'message'=>HandleMessage::activeMsg(7)]);
            endif;
            return response()->json(HandleMessage::errActMsg(8),500);
            
        }
        catch(\PDOException | Exception $e){
            return response()->json(['errors'=>$e->getMessage()],500);
        }

    }

    public function destroy(ActiveUser $activeUser){
        if(!request()->isJson()):
            return response()->json(HandleMessage::unAuth,401);
        endif;

        if($activeUser->delete()):
            return response()->json(['message'=>HandleMessage::activeMsg(11)],200);
        endif;
        return response()->json(HandleMessage::errActMsg(12),500);
    }
}
