<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\FileController;
use App\Status;
use Illuminate\Support\Facades\Validator;
use Exception;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function  __construct(User $user,Status $status)
     {
         $this->user = $user;
         $this->status = $status;
         $this->userRequest = new UserRequest();
         $this->fileController = new FileController();
     }
     
    public function index()
    {
        if(request()->isJson()):
            return response()->json(['users'=> $this->user->userList()]);
        endif;
        return response()->json("Unauthorized",401);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->isJson()):
            return response()->json("Unauthorized",401);
        endif;

        try
        {    
            $user = strlen($request->image) > 255 ? $request->except('image') : $request->all();
            $image = $request->image;

            if(!$path = $this->fileController->storeImage($image)):
                return response()->json(["errors" => "Error al ingresar el usuario, archivo imagen no admitido"],400);
            endif;

            $user['image'] = $path;
            
            $validator = Validator::make($user,$this->userRequest->rules(),$this->userRequest->messages());
            
            if($validator->fails()):
                return response()->json(["errors" => $validator->errors()],400);
            endif;

            if(User::create($user)):
                return response()->json(["user"=> $this->user->getUser($user),"message" => "Usuario ingresado correctamente",'status'=>200]);
            else:
                return response()->json(["message" => "Error al ingresar el usuario"],400);
            endif;
           
        }   
        catch (\PDOException | Exception $e){
            return response()->json($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,User $user)
    {
        if(!$request->isJson()):
            return response()->json("Unauthorized",401);
        endif;

        try{
            
            $data = strlen($request->image) > 255 ? $request->except('image') : $request->all();
            $image = $request->image;
            if(!$path = $this->fileController->updateImage($image,$user->getAttributes())):
                return response()->json(["errors" => "Error al ingresar el usuario, archivo imagen no admitido"],400);
            endif;

            $data['image'] = $path;
            $data['status_id'] = is_array($data['status_id']) ? $data['status_id']['id'] : $data['status_id']; // array string conversion is because, the props of array has another props inside... we need to have carefull because it could cause problems in a future

            $validator = Validator::make($data,$this->userRequest->rules($user,true),$this->userRequest->messages());
            
            if($validator->fails()):
                return response()->json(['errors'=>$validator->errors()],400);
            endif;

            if($user->update($data)):
                return response()->json(['message'=>"Usuario modificado correctamente",'user'=>$this->user->getUser($data)]);
            else:
                return response()->json(['user'=>"Error al modificar el usuario $request->first_name",'status'=>500]);
            endif;
        }
        catch (\PDOException | Exception $e){
            return response()->json([$e->getMessage(),$e->getCode(), $data['status_id']],400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if(request()->isJson()):
            try 
            {
                if($user->delete()):
                    return response()->json(['message'=>'Usuario eliminado correctamente!']);
                else:
                    return response()->json(['message'=>'Error al eliminar usuario'],400); 
                endif;
            } 
            catch (\PDOException | Exception $e) 
            {
                return response()->json(['users'=>[],'message'=>$e->getMessage()],401);
            }
        else:
            return response()->json("Unauthorized",401);
        endif;
    }

    public function disable(Request $request)
    {
        if($request->isJson()):
            $data = $request->all();
            $userId = $request->all()['user']['id'];
            $user = User::find($userId);
            $user->status_id = $data['disableUser'] ? Status::active : Status::no_active;

            try {
                if ($user->save()):
                    return response()->json(['message'=>'Usuario deshabilitado correctamente', 'user'=>$this->user->getUser($user)]);
                else:
                    return response()->json(['message'=>'Error al deshabilitar el usuario']);
                endif;    
            } catch(\PDOException | Exception $e) {
                return response()->json(['errors'=>$e->getMessage()],500);
            }
        else:
            return response()->json("Unauthorized",401);
        endif;
    }

    public function filterUser(Request $request)
    {
        if(request()->isJson()):
            
            $valueFilter = $request->all()['valueFilter'];

            switch ($valueFilter) {
                case 1:
                    return response()->json(['users'=>$this->user->userList()]);
                    break;
                case 2:
                    return response()->json(['users'=>$this->user->getUserActive()]);
                    break;
                case 3:
                    return response()->json(['users'=>$this->user->getUserDesactive()]);
                    break;
                default:
                    return response()->json(['users'=>$this->user->userList()]);
                    break;
            }
        else:
            return response()->json("Unauthorized",401);
        endif; 
    }
}
