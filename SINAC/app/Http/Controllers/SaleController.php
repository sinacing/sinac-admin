<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sale;
use Exception;
use PDOException;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->sale = new Sale();
     
    }
    public function index()
    {
        if(!request()->isJson()):
            return response()->json("Unauthorized",401);
        endif;            
        return response()->json(['sales'=> $this->sale->salesList()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->isJson()):
            return response()->json("Unauthorized",401);
        endif;

        try{
            $sale = $request->all();
            
            if($idSale = Sale::create($sale)):
                // ya funciona hasta aca
            else:
                return response()->json(['message'=>'No se ha efectuado la venta'],400);
            endif;
        }
        catch(PDOException | Exception $e){
            return response()->json(['errors'=>$e->getMessage()],500);
        }
            
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Sale::destroy($id);
    }
}
