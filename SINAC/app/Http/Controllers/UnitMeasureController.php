<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UnitMeasure;
use Exception;

class UnitMeasureController extends Controller
{   
    public function index(){
        if(!request()->isJson()):
            return response()->json("Unauthorized",401);
        endif;
        
        try{
            return response()->json(['unitmeasures'=>UnitMeasure::all()],200);
        }
        catch (\PDOException | Exception $e){
            return response()->json(['errors'=>$e->getMessage()],500);
        }

    }
}
