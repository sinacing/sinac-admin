<?php

namespace App\Http\Controllers;

use App\Category;
use App\Subcategory;
use Illuminate\Http\Request;
use PDOException;
use Exception;
use App\Http\Requests\CategoryRequest;
use Illuminate\Support\Facades\Validator;
use App\HandleMessage;

class CategoryController extends Controller
{

    public function __construct()
    {
        $this->category = new Category();
        $this->validCategory = new CategoryRequest();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!request()->isJson()):
            return response()->json(HandleMessage::unAuth,401);
        endif;

        try{
            return response()->json(['categories'=>Category::all()]);
        }catch(PDOException | Exception $e){
            return response()->json(['errors'=>$e->getMessage()],500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!request()->isJson()):
            return response()->json(HandleMessage::unAuth,401);
        endif;
        try{
            $category = $request->all();
            $validator = Validator::make($category,$this->validCategory->rules(),$this->validCategory->messages());
            
            if($validator->fails()):
                return response()->json(['errors'=> $validator->errors()],400);
            endif;

            if($categoryResult = Category::create($category)):
                return response()->json(["category" => $categoryResult ,"message" =>HandleMessage::categoryMsg(1)],201);
            endif;
            return response()->json(HandleMessage::errCategoryMsg(2),500);
            
            
        }catch(PDOException | Exception $e){
            return response()->json(['errors'=>$e->getMessage()],500);
        }
            
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Category $category)
    {
        if(!$request->isJson()):
            return response()->json(HandleMessage::unAuth,401);
        endif;
        try{
            $data = $request->all();
            $validator = Validator::make($data,$this->validCategory->rules($category,true),$this->validCategory->messages());

            if($validator->fails()):
                return response()->json(['errors'=> $validator->errors()],400);
            endif;
            
            if($categoryResult = tap($category)->update($data)): 
                return response()->json(["category" => $categoryResult ,"message" => HandleMessage::categoryMsg(3)],200);
            endif;
            return response()->json(HandleMessage::errCategoryMsg(4),500);

        }catch(PDOException | Exception $e){
            return response()->json(['errors'=>$e->getMessage()],500);
        }     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        if(!request()->isJson()):
            return response()->json(HandleMessage::unAuth,401);
        endif;

        $subcategory = Subcategory::all()->where('category_id',$category->id);

        try{
            if($subcategory->count() > 0):
                return response()->json(['message'=>'La Partida está en uso'],400);
            endif;
            return $category->delete() ? response()->json(['message'=>HandleMessage::categoryMsg(5)]) : response()->json(HandleMessage::errCategoryMsg(6),400);
            
        }catch (PDOException | Exception $e) {
            response()->json(['errors'=>$e->getMessage()],500);
        }
    }
}
