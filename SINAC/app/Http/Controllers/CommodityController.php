<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Commodity;
use App\Http\Requests\CommodityRequest;
use App\Http\Controllers\QuantityController;
use App\Status;
use Exception;
use Illuminate\Support\Facades\Validator;

class CommodityController extends Controller
{
       
    public function __construct()
    {
        $this->commodity = new Commodity();
        $this->commodityRequest = new CommodityRequest();
        $this->quantityController = new QuantityController();
    }

    public function index()
    {
        if(!request()->isJson()):
            return response()->json("Unauthorized",401);
        endif;
        return response()->json(['commodities'=> $this->commodity->commodityList()]);
            
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->isJson()):
            return response()->json("Unauthorized",401);
        endif;

        try{
            $commodity = $request->all(); 
            $quantity_object_response = $this->quantityController->store($commodity['quantity_id']); // vamos a proceder a guardar la cantidad
            if(!$quantity_object_response['status']):
                return response()->json($quantity_object_response['errors'],400);
            endif;
            unset($commodity['stock'],$commodity['quanty'],$commodity['total_quanty'],$commodity['unit_measure_id'],$commodity['type_commodity_id']); // sacamos dichas propiedades del array
            
            $commodity['quantity_id'] = $quantity_object_response['quantity_id'];
            $commodity['status_id'] = is_array($commodity['status_id']) ? $commodity['status_id']['id'] : $commodity['status_id'];
            $commodity['warehouse_id'] = is_array($commodity['warehouse_id']) ? $commodity['warehouse_id']['id'] : $commodity['warehouse_id'];
            $commodity['subcategory_id'] = is_array($commodity['subcategory_id']) ? $commodity['subcategory_id']['id'] : $commodity['subcategory_id'];

            $validator = Validator::make($commodity,$this->commodityRequest->rules(),$this->commodityRequest->messages());

            if($validator->fails()):
                $this->quantityController->destroy($commodity['quantity_id']);
                return response()->json(["errors" => $validator->errors()],400);
            endif;

            if(Commodity::create($commodity)):
                return response()->json(['commodity'=>$this->commodity->getCommodity($commodity),'message'=>'Artículo ingresado correctamente'],201);
            else:
                return response()->json(['message'=>'Error al ingresar el artículo'],400);
            endif;
        }
        catch(\PDOException | Exception $e){
            $this->quantityController->destroy($commodity['quantity_id']); // en caso de fallo de guardado 
            return response()->json(['errors'=>$e->getMessage()],500);
        }            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Commodity $commodity)
    {
        if(!$request->isJson()):
            return response()->json("Unauthorized",401);
        endif;
        
        $data = $request->all();

        try {
            $quantity_object_response = $this->quantityController->update($data['quantity_id']); // ya actualiza, pero debemos mejorar la forma, mucho codigo

            if(!$quantity_object_response['status']):
                return response()->json($quantity_object_response['errors'],400);
            endif;
            
            $data['quantity_id'] = is_array($data['quantity_id']) ? $data['quantity_id']['id'] : $data['quantity_id'];
            $data['status_id'] = is_array($data['status_id']) ? $data['status_id']['id'] : $data['status_id'];
            $data['warehouse_id'] = is_array($data['warehouse_id']) ? $data['warehouse_id']['id'] : $data['warehouse_id'];
            $data['subcategory_id'] = is_array($data['subcategory_id']) ? $data['subcategory_id']['id'] : $data['subcategory_id'];

            $validator = Validator::make($data,$this->commodityRequest->rules($commodity,true),$this->commodityRequest->messages());

            if($validator->fails()):
                return response()->json(["errors" => $validator->errors()],400);
            endif;
            
            if($commodity->update($data)): // el modoifica apartir del original
                return response()->json(['commodity'=>$this->commodity->getCommodity($data),'message'=>'Artículo modificado correctamente'],200);
            else:
                return response()->json(['message'=>'Error al modificar el artículo'],400);
            endif;
        }catch(\PDOException | Exception $e) {
            return response()->json(['errors'=>$e->getMessage()],500);
        }
            
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function disable(Request $request)
    {
        if(!request()->isJson()):
            return response()->json("Unauthorized",401);
        endif;

        $data = $request->all();

        $commodity = Commodity::find($request->article);
        $commodity->status_id = $data['disableArticle'] ? Status::active : Status::no_active;

        try {
            if($commodity->save()):
                return response()->json(['message'=>'Artículo deshabilitado correctamente','commodity'=>$this->commodity->getCommodity($commodity)]);
            else:
                return response()->json(['message'=>'Error al deshabilitar el artículo']);
            endif;  
        }catch(\PDOException | Exception $e) {
            return response()->json(['errors'=>$e->getMessage()],500);
        }            
        
    }

    public function filterCommodity(Request $request){
        if(!request()->isJson()):
            return response()->json("Unauthorized",401);
        endif;
        
        $valueFilter = $request->all()['valueFilter'];

        switch ($valueFilter) {
            case 1:
                return response()->json(['commodities'=>$this->commodity->commodityList()]);
                break;
            case 2:
                return response()->json(['commodities'=>$this->commodity->getCommodityActive()]);
                break;
            case 3:
                return response()->json(['commodities'=>$this->commodity->getCommodityDesactive()]);

                break;
            case 4:
                return response()->json(['commodities'=>$this->commodity->getCommodityLowStock()]);

                break;
            default:
                return response()->json(['commodities'=>$this->commodity->commodityList()]);
                break;
        }            
    }
}
