<?php

namespace App\Http\Controllers;

use App\Role;
use App\RoleUser;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDOException;

class RoleUserController extends Controller
{
    public function __construct()
    {   
        $this->user = new User();
        $this->roleUser = new RoleUser();
    }

    public function index()
    {
        if(!request()->isJson()):
            return response()->json("Unauthorized",401);
        endif;
        return response()->json(['role_users'=> $this->user->administratorEmployee()]);
            
    }

    public function storeAccepted($roleUser){

        if(RoleUser::create($roleUser)):
            return true;
        endif;
        return false;
    }

    public function store($roles, $rolesUser,$id){
        $count = 0;
        try{
            for($i=0; $i<sizeof($roles); $i++):
                if(!in_array($roles[$i],$rolesUser)):
                    RoleUser::create(['user_id'=>$id,'role_id'=>$roles[$i]]) ? $count ++ : $count; // en caso de que venga la lista con un nuevo role que no este sobre la lista existente
                endif;   
            endfor;
            return $count;
        }
        catch (PDOException | Exception $e) {
            return response()->json(['message'=>'Error al eliminar roles'.$e->getMessage()],400);
        }
    }

    public function modify(Request $request,$id){
       
        if(request()->json()):
            try {
                $count = 0;
                $roles =  $request->all()['roles']; // capturamos los roles del usuario que vienen desde el frontend
                $rolesUser = $this->extractRoles($id);

                for($x = 0; $x < sizeof($rolesUser); $x++):
                    if(!in_array($rolesUser[$x], $roles) && $rolesUser[$x] != Role::administratorEmployee):
                        $roleDelete = $this->roleUser->all()->where('user_id',$id)->where('role_id',$rolesUser[$x])->first(); 
                        $roleDelete->delete() ? $count ++ : $count;
                    endif;
                endfor;
                
                $count += $this->store($roles,$rolesUser,$id);
                return response()->json(["message"=>"Roles modificados correctamente $count","role"=>$this->roleUser->getRolesUser($id)]);

            } catch (PDOException | Exception $e) {
               return response()->json(['message'=>'Error al eliminar los roles'.$e->getMessage()],400);
            }
        else:
            return response()->json("Unauthorized",401);
        endif;
        
    }
    public function extractRoles($userId){
        $listRoles = [];
        $roles = DB::select('select role_id from role_users where user_id = ?',[$userId]);
        foreach($roles as $value):
            array_push($listRoles,$value->role_id);
        endforeach;
        return $listRoles;
    }
    // roles
    public function roles(){
        if(request()->isJson()):
            return response()->json(['roles'=> Role::all()]);
        else:
            return response()->json("Unauthorized",401);
        endif;
    }

}
