<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use File;

class FileController extends Controller
{

    public $defaultPath = '/images/user/default.jpg';

    public function storeImage($image){

        if(strlen($image) <= 255):
            return $image; // return default or edit path
        endif;

        $image_exploded = explode(',',$image);

        if(str_contains($image_exploded[0],['jpeg','jpg','png'])):
            
            $extension = $this->get_extension($image_exploded[0]);
            $image = base64_decode($image_exploded[1]);
            $file_name = $this->get_name_file($image_exploded[0]).'.'.$extension;
            
            (!file_exists(public_path().'/images/user/')) ? mkdir(public_path().'/images/user/',0777,true) : null;

            $path = '/images/user/'.$file_name;

            return file_put_contents(public_path().$path,$image) ? $path : false;
        endif;

        return false;
    }

    public function updateImage($image,$user = []){
        
        if(strlen($image) <= 255) return $image;

        $this->deleteImg($user['image']);

        return $this->storeImage($image);
        
    }

    public function deleteImg($imageRoute){
        try{
            if($imageRoute != $this->defaultPath):
                return unlink(public_path().$imageRoute);
            endif;
        }
        catch(Exception $e){
            return false;
        }
    }

    private function get_extension($extension){
        $ext = null;
        if(strpos($extension,'jpeg')):
            $ext = 'jpeg';
        elseif(strpos($extension,'jpg')):
            $ext = 'jpg';
        else:
            $ext = 'png';
        endif;

        return $ext;
    }

    private function get_name_file(){
        return time().'_'.date('Y_m_d');
    }

}
