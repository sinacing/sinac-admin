<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subcategory;
use App\Commodity;
use Exception;
use PDOException;
use App\Http\Requests\SubcategoryRequest;
use Illuminate\Support\Facades\Validator;


class SubCategoryController extends Controller
{
    public function __construct()
    {
        $this->subcategory = new Subcategory();
        $this->validateSubcategory = new SubcategoryRequest();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!request()->isJson()):
            return response()->json("Unauthorized",401);            
        endif;
        return response()->json(['subcategories'=>$this->subcategory->getSubcategories()]);
    }
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->isJson()):
            return response()->json('Unauthorized',401);
        endif;

        try{
            $subcategory = $request->all();
            $validator = Validator::make($subcategory,$this->validateSubcategory->rules(),$this->validateSubcategory->messages());
            
            if($validator->fails()):
                return response()->json(['errors'=> $validator->errors()],400);
            endif;

            if($this->subcategory->create($subcategory)): 
                return response()->json(["subcategory" => $this->subcategory->getSubcategory($subcategory) ,"message" => "Subpartida ingresada correctamente"],201);
            else:
                return response()->json(["subcategory" =>[] ,"message" => "Error al ingresar la subpartida"],500);
            endif;

        }
        catch(PDOException | Exception $e){
            return response()->json(['error'=>$e->getMessage()],500);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subcategory $subcategory)
    {
        if(!$request->isJson()):
            return response()->json('Unauthorized',401);
        endif;

        try{
            $data = $request->all();
            $data['category_id'] = is_array($data['category_id']) ? $data['category_id']['id'] : $data['category_id'];
            
            $validator = Validator::make($data,$this->validateSubcategory->rules($subcategory,true),$this->validateSubcategory->messages());
            
            if($validator->fails()):
                return response()->json(['errors'=> $validator->errors()],400);
            endif;

            if($subcategory->update($data)): 
                return response()->json(["subcategory" => $this->subcategory->getSubcategory($data) ,"message" => "Subpartida modificada correctamente"],201);
            else:
                return response()->json(["subcategory" =>[] ,"message" => "Error al modificar la subpartida"],500);
            endif;

        }
        catch(PDOException | Exception $e){
            return response()->json(['error'=>$e->getMessage()],500);
        }            
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subcategory $subcategory)
    {
        $subcategory_id = $subcategory->id;
        $commodity_sub = Commodity::where("subcategory_id","=",$subcategory_id)->select('id');

        if(!request()->isJson()):
            return response()->json("Unauthorized",401);
        endif;
        
        try{
            if($commodity_sub->count() > 0 ){
                return response()->json(['message'=>'La Sub-partida está en uso! '],400);
            }

            if($subcategory->delete()):
                return response()->json(['message'=>'Sub-partida eliminada correctamente!']);
            else:
                return response()->json(['message'=>'Ha ocurrido un error al eliminar la subpartida!'],400);
            endif;
        }
        catch(PDOException | Exception $e){
            return response()->json(['error'=>$e->getMessage()],500);
        }
           
    }
}
