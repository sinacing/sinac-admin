<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;
use Exception;

class StatusController extends Controller
{
    public function __construct(Status $status)
    {
        $this->status = $status;
    }

    public function index(){

        if(!request()->isJson()):
            return response()->json("Unauthorized",401);
        endif;
        
        try{
            return response()->json(['statuses'=>$this->status->all()]);
        }
        catch (\PDOException | Exception $e){
            return response()->json($e->getMessage());
        }
    }
}
