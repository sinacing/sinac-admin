<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CompanySupplier;
use App\HandleMessage;
use App\Http\Requests\CompanySupplierRequest;
use App\Status;
use PDOException;
use Exception;
use Illuminate\Support\Facades\Validator;



class CompanySupplierController extends Controller
{
    
    public function __construct()
    {
        $this->companySupplierRequest = new CompanySupplierRequest();
        $this->companySupplier = new CompanySupplier();
    }

    public function index()
    {
        return request()->isJson() ? response()->json(['company_supplier'=> $this->companySupplier->getAllCompanySupplier()]) :
                                    response()->json(HandleMessage::unAuth,401);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->isJson()):
            response()->json(HandleMessage::unAuth,401);
        endif;

        try {
            $supplier = $request->all();

            $validator = Validator::make($supplier, $this->companySupplierRequest->rules(), $this->companySupplierRequest->messages());
            if($validator->fails()):
                return response()->json(["errors" => $validator->errors()],400);
            endif;

            if($result = CompanySupplier::create($supplier)):
                return response()->json(['company_supplier'=>$result, 'message'=>HandleMessage::supplMsg(1)],201);
            endif;
            return response()->json(HandleMessage::errSupplMsg(2),500);

        }catch(PDOException | Exception $e){
            return response()->json(['errors'=>$e->getMessage()],500);
        } 
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanySupplier $companySupplier)
    {
        if(!$request->isJson()):
            response()->json(HandleMessage::unAuth,401);
        endif;

        try {
            $data = $request->all();
            $validator = Validator::make($data,$this->companySupplierRequest->rules($companySupplier,true), $this->companySupplierRequest->messages());

            if($validator->fails()):
                return response()->json(["errors" => $validator->errors()],400);
            endif;

            if($result = tap($companySupplier)->update($data)):
                return response()->json(['company_supplier'=> $this->companySupplier->getCompanySupplier($result), 'message'=>HandleMessage::supplMsg(3)]);
            endif;
            return response()->json(HandleMessage::errSupplMsg(4),500);
        }catch(PDOException | Exception $e){
            return response()->json(['errors'=>$e->getMessage()],500);
        } 
    }

    public function disable(Request $request){
        if(!$request->isJson()):
            response()->json(HandleMessage::unAuth,401);
        endif;

        try{
            $data = $request->all();
            $companySupplier = CompanySupplier::find($data['id']);
            // here send only 1 = true, 0 = false
            $companySupplier->status_id = $data['status_id'] ? Status::active : Status::no_active;

            if($companySupplier->save()):
               return response()->json(['company_supplier'=> $this->companySupplier->getCompanySupplier($companySupplier)],200);
            endif;
            return response()->json(HandleMessage::errSupplMsg(4),500);

        }catch(PDOException | Exception $e){
            return response()->json(['errors'=>$e->getMessage()],500);
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
