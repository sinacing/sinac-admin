<?php

namespace App\Http\Controllers;

use App\Active;
use Illuminate\Http\Request;
use App\Area;
use App\Http\Requests\AreaRequest;
use Illuminate\Support\Facades\Validator;
use App\User;
use Exception;
use App\HandleMessage;

class AreaController extends Controller
{
    public function __construct()
    {
        $this->area = new Area();
        $this->areaRequest = new AreaRequest();
    }

    public function index(){

        if(!request()->isJson()):
            return response()->json(HandleMessage::unAuth,401);
        endif;

        try{
            return response()->json(['areas'=>Area::all()]);
        }
        catch (\PDOException | Exception $e){
            return response()->json(['errors'=>$e->getMessage()],500);
        }
            
    }

    public function store(Request $request)
    {
        if(!$request->isJson()):
            return response()->json(HandleMessage::unAuth,401);  
        endif;

        try{
            $area = $request->all();
            $validator = Validator::make($area,$this->areaRequest->rules(),$this->areaRequest->messages());

            if($validator->fails()):
                return response()->json(['errors'=>$validator->errors()],400);
            endif;
            
            if($areaResult = Area::create($area)):
                return response()->json(['area'=>$areaResult,'message'=>HandleMessage::areaMsg(1)]);
            endif;
            return response()->json(HandleMessage::errAreaMsg(2),500);
            
        } 
        catch (\PDOException | Exception $e){
            return response()->json(['errors'=>$e->getMessage()],500);
        }
            
    }

    public function update(Request $request,Area $area)
    {
        if(!$request->isJson()):
            return response()->json(HandleMessage::unAuth,401);  
        endif; 
        
        try{
            $data = $request->all();
            $validator = Validator::make($data,$this->areaRequest->rules($area,true),$this->areaRequest->messages());
            if($validator->fails()):
                return response()->json(['errors'=>$validator->errors()],400);
            endif;
            
            if($areaResult = tap($area)->update($data)):
                return response()->json(['area'=>$areaResult,'message'=>HandleMessage::areaMsg(3)]);
            endif;
            return response()->json(HandleMessage::errAreaMsg(4),500);
            
        }
        catch (\PDOException | Exception $e){
            return response()->json(['errors'=>$e->getMessage()],500);
        }

    }

    public function destroy(Area $area)
    {
        
        if(!request()->isJson()):
            return response()->json(HandleMessage::unAuth,401);  
        endif;
        try
        {
            if(!$this->validateDelete($area->id)):
                return response()->json(['message'=>'Departamento se encuentra en uso'],400);
            endif;

            if($area->delete()):
                return response()->json(['message'=>HandleMessage::areaMsg(5)]);
            endif;
            return response()->json(HandleMessage::errAreaMsg(6),500);
            
        }
        catch (\PDOException | Exception $e){
            return response()->json(['errors'=>$e->getMessage()],500);
        }            
    }

    public function validateDelete($areaId){
        $userArea = User::all()->where('area_id',$areaId);
        $activeArea = Active::all()->where('area_id',$areaId);
        
        return (sizeof($userArea) == 0 && sizeof($activeArea) == 0) ? true : false;
    }
}
