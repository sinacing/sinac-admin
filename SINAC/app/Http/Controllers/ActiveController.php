<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Active;
use App\Http\Requests\ActiveRequest;
use App\Status;
use Exception;
use App\HandleMessage;

class ActiveController extends Controller
{
    // TODO realizar en la db los cambios de subpartida y bodega

    public function __construct()
    {
        $this->active = new Active();
        $this->status = new Status();
        $this->activeRequest = new ActiveRequest();       
    }

    public function index()
    {
        if(request()->isJson()):
            return response()->json(['actives'=> $this->active->getActives()]);
        endif;
        return response()->json(HandleMessage::unAuth,401);
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
          
        if(!$request->isJson()):
            return response()->json(HandleMessage::unAuth,401);
        endif;

        try{
            
            $active = $request->all(); 
        
            $validator = Validator::make($active,$this->activeRequest->rules(),$this->activeRequest->messages());

            if($validator->fails()):
                return response()->json(["errors" => $validator->errors()],400);
            endif;

            if($activeResult = Active::create($active)):
                return response()->json(['active'=> $activeResult ,'message'=>HandleMessage::activeMsg(1)]);
            endif;
            return response()->json(HandleMessage::errActMsg(2),500);
        }
        catch(\PDOException | Exception $e){
            return response()->json(["errors" => $e->getMessage()],500);
        }            

    }
    public function disable(Request $request)
    {
        if(!request()->isJson()):    
            return response()->json(HandleMessage::unAuth,401);
        endif;

        $active = Active::find($request->active);
        $active->status_id = $request->disableActive ? Status::active : Status::no_active;

        try {
            if($active->save()):
                return response()->json(['message'=>HandleMessage::activeMsg(9),'actives'=>$active->getAttributes()],201);
            endif;
            return response()->json(HandleMessage::errActMsg(10),500);
              
        }catch(\PDOException | Exception $e) {
            return response()->json(['errors'=>$e->getMessage()],500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Active $active)
    {
        if(!$request->isJson()):
            return response()->json(HandleMessage::unAuth,401);
        endif;
        try{
            $data = $request->all();
            $data['status_id'] = is_array($data['status_id']) ? $data['status_id']['id'] : $data['status_id'];
            $data['subcategory_id'] = is_array($data['subcategory_id']) ? $data['subcategory_id']['id'] : $data['subcategory_id'];
            $data['warehouse_id'] = is_array($data['warehouse_id']) ? $data['warehouse_id']['id'] : $data['warehouse_id'];
            
            $validator = Validator::make($data,$this->activeRequest->rules($active,true),$this->activeRequest->messages());

            if($validator->fails()):
                return response()->json(['errors'=>$validator->errors()],400);
            endif;

            if($activeResult = tap($active)->update($data)):
                return response()->json(['active'=>$activeResult,'message'=>HandleMessage::activeMsg(3)]);
            endif;
            return response()->json(HandleMessage::errActMsg(4),400);
            
        }
        catch(\PDOException | Exception $e){
            return response()->json(['errors'=>$e->getMessage()],400);
        }
            
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Active $active)
    {
        // validar si el activo esta ocupado
        if(!request()->isJson()):
            return response()->json(HandleMessage::unAuth,401);
        endif;

        try 
        {
            return $active->delete() ? response()->json(['message'=>HandleMessage::activeMsg(5)]) :
                                     response()->json(HandleMessage::errActMsg(6),500);
            
        } 
        catch (\PDOException | Exception $e) 
        {
            return response()->json(['errors'=>$e->getMessage()],500);
        }
    }

    public function filterActive(Request $request){
        if(!request()->isJson()):
            return response()->json(HandleMessage::unAuth,401);
        endif;
            
        $valueFilter = $request->all()['valueFilter'];
        return $this->active->filterStatusActive($valueFilter);

    }
}
