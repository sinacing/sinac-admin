<?php

namespace App\Http\Controllers;

use App\Http\Requests\QuantityRequest;
use Illuminate\Http\Request;
use App\Quantity;
use Exception;
use Illuminate\Support\Facades\Validator;
use PDOException;
use Illuminate\Support\Facades\DB;

class QuantityController extends Controller
{
    public function __construct()
    {
        $this->quantity = new Quantity();
        $this->quantityRequest = new QuantityRequest();
    }

    public function index(){

        if(!request()->isJson()):
            return response()->json("Unauthorized",401);
        endif;
        try{
            return response()->json(['quantity'=>$this->quantity->quantityList()]);
        }
        catch (\PDOException | Exception $e){
            return response()->json($e->getMessage());
        }           
    }
    
    public function store($commodity)
    {
        try{ 
            $quantity = new Quantity($commodity['stock'],$commodity['quanty'],$commodity['total_quanty'],$commodity['unit_measure_id'],$commodity['type_commodity_id']);
            $validator = Validator::make($quantity->getAttributes(),$this->quantityRequest->rules(),$this->quantityRequest->messages());

            if($validator->fails()):
                return ['status'=>false,"errors" => $validator->errors()];
            endif;
                
            if($quantity->save()):
                return ['status'=>true,'quantity_id'=>$quantity->id];
            else:
                return ['status'=>false,'errors'=>'Error al guardar la cantidad']; // fallo
            endif;
        }
        catch(PDOException | Exception $e){
            return ['status'=>false,'errors'=>$e->getMessage()]; // validar que no ingrese negativos
        }
    }

    public function update($quantity){

        try{
            $data = $this->quantity->findOrFail($quantity['id']); 

            $validator = Validator::make($quantity,$this->quantityRequest->rules(),$this->quantityRequest->messages());
            if($validator->fails()):
                return ['status'=>false,"errors" => $validator->errors()];
            endif;

            if($data->update($quantity)):
                return ['status'=>true,'quantity_id'=>$data->id];
            else:
                return ['status'=>false,'errors'=>'Error al modificar la cantidad']; // fallo
            endif;
        }
        catch(PDOException | Exception $e){
            return ['status'=>false,'errors'=>$e->getMessage()]; // validar que no ingrese negativos
        }
    }

    public function destroy($id)
    {
                                        // hacemos un esfuerzo nuevo
        Quantity::destroy($id) ? null : DB::delete('delete quantities where id = ?', [$id]);
    }
}


/**  Ya esta retornando el id de la cantidad solo falta obtenerlo en el otro controlador y validar, despues solo procedemos a guardar el ariculo */