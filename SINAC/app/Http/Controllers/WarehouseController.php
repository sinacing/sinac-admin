<?php

namespace App\Http\Controllers;

use App\Warehouse;
use App\Commodity;
use Illuminate\Http\Request;
use PDOException;
use Exception;
use App\Http\Requests\WarehouseRequest;
use Illuminate\Support\Facades\Validator;

class WarehouseController extends Controller
{
    function __construct()
    {
        $this->warehouse = new Warehouse();
        $this->validateWarehouse = new WarehouseRequest();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!request()->isJson()):
            return response()->json('Unauthorized',401);
        endif;
        
        try{
            return response()->json(['warehouses'=>Warehouse::all()]);
        }catch(PDOException | Exception $e){
            return response()->json(['error'=>$e->getMessage()],500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->isJson()):
            return response()->json('Unauthorized',401);
        endif;
        try{
            $warehouse = $request->all();
            $validator = Validator::make($warehouse,$this->validateWarehouse->rules(),$this->validateWarehouse->messages());
            
            if($validator->fails()):
                return response()->json(['errors'=>$validator->errors()],400);
            endif;
            
            if($this->warehouse->create($warehouse)):
                return response()->json(['warehouse'=>$this->warehouse->getWarehouse($warehouse),'message'=>'Bodega ingresada correctamente']);
            endif;

            return response()->json(['warehouse'=>[],'message'=>'Error al ingresar la bodega'],400);
            
        }catch (PDOException | Exception $e) {
            return response()->json(['error'=>$e->getMessage()],500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Warehouse $warehouse)
    {
        if(!$request->isJson()):
            return response()->json('Unauthorized',401);
        endif;

        try{
            $data = $request->all();
            $validator = Validator::make($data,$this->validateWarehouse->rules($warehouse,true),$this->validateWarehouse->messages());

            if($validator->fails()):
                return response()->json(['errors'=>$validator->errors()],400);
            endif;

            if($warehouse->update($data)):
                return response()->json(['warehouse'=>$this->warehouse->getWarehouse($data),'message'=>'Bodega modificada correctamente']);
            endif;
            return response()->json(['warehouse'=>[],'message'=>'Error al modificar la bodega'],400);
            

        }catch (PDOException | Exception $e) {
            return response()->json(['errors'=>$e->getMessage()],500);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Warehouse $warehouse)
    {  
        $warehouse_id = $warehouse->id;
        $Commodity = Commodity::where('warehouse_id','=',$warehouse_id)->select('warehouse_id');
  
     
        if(!request()->isJson()):
            return response()->json('Unauthorized',401);
        endif;

        try{
            if($Commodity->count() > 0)
            {
                return response()->json(['message'=>'La bodega está en uso'],400);
            }
            
            if($warehouse->delete()):
                return response()->json(['message'=>'Bodega eliminada correctamente']);
            endif;
            return response()->json(['message'=>'Error al eliminar la bodega'],400);

        }catch (PDOException | Exception $e) {
            response()->json(['errors'=>$e->getMessage()],500);
        }
    }
}
