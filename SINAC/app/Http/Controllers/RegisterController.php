<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use App\Http\Requests\UserRequest;
use App\Register;
use App\User;
use App\Role;
use App\Status;
use Exception;
use Illuminate\Http\Request;
use PDOException;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{

    function __construct()
    {
        $this->registerRequest = new RegisterRequest();
        $this->userRequest = new UserRequest();
        $this->register = new Register();
        $this->roleUserController = new RoleUserController();
        $this->user = new User();
        $this->middleware('jwt.auth')->except('store');
    }

    public function index(){
        if(!request()->isJson()):
            return response()->json(['error'=>'Unauthorized'],401);
        endif;
        return response()->json(['registers'=>$this->register->getRegisters()]);
            
    } 

    public function store(Request $request){
        if(!$request->isJson()):
            return response()->json(['error'=>'Unauthorized'],401);
        endif;

        try
        {
            $data = $request->all();
            
            $validator = Validator::make($data, $this->registerRequest->rules(),$this->registerRequest->messages());
            if($validator->fails()):
                return response()->json(["errors" => $validator->errors()],400);
            endif;

            if(Register::create($data)):
                return response()->json(['message'=>'Solicitud registrada correctamente'],201);
            endif;
            return response()->json(['message'=>"Error al procesar la solicitud",400]);
            
        }
        catch(PDOException | Exception $e)
        {
            return response()->json(['errors'=>$e->getMessage()],400);
        } 
    }

    public function acceptedRegister(Request $request){
        if(!$request->isJson()):
            return response()->json(['error'=>'Unauthorized'],401);
        endif;

        try
        {
            $registry = Register::find($request['id'])->getAttributes();
            $id = $registry['id']; 
            $role = Role::administratorEmployee;
            unset($registry['id'],$registry['role_id'],$registry['created_at'],$registry['updated_at']);
            $registry['status_id'] = Status::active;
            
            if($this->validateDataUser($registry)):
                return response()->json(['errors'=>$this->validateDataUser($registry)],400);
            endif;

            if($idUser = User::create($registry)):
                $this->roleUserController->storeAccepted(['user_id'=>$idUser['id'],'role_id'=>$role]);
                $this->destroy($id);
                return response()->json(['message'=>'Solicitud aprobada','user'=>$this->user->getUser($idUser)]);
            else:
                return response()->json(['message'=>'Error al aceptar la solicitud del usuario'],400);
            endif;
        }
        catch(PDOException | Exception $e)
        {
            return response()->json(['errors'=>$e->getMessage()],400);
        }
    }  
    
    public function destroy($id)
    {
        if(!request()->isJson()):
            return response()->json(['error'=>'Unauthorized'],401);
        endif;
        
        try{
            if(Register::destroy($id)):
                return response()->json(['message'=>'Solicitud rechazada']);
            endif;
            return response()->json(['message'=>'Error al eliminar la solicitud del usuario'],400);
            
        }
        catch(PDOException | Exception $e)
        {
            return response()->json(['errors'=>$e->getMessage()],400);
        }           
    }
    
    function validateDataUser($data){
        $validator = Validator::make($data,$this->userRequest->rules(),$this->userRequest->messages());
        if($validator->fails()):
            return $validator->errors();
        endif;
        return false;
    }

}
