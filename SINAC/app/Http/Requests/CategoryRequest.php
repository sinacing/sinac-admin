<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules($category = [],$isUpdate = false)
    {
        
        if($isUpdate):
            return [
                'name' => ['max:50',Rule::unique('categories')->ignore($category->id)],
                'code' => ['max:20',Rule::unique('categories')->ignore($category->id)],
                'description' => 'required|max:100'
            ];
        endif;
        
        return [
            'name' => ['max:50','unique:categories,name'],
            'code' => ['max:20','unique:categories,code'],
            'description' => 'required|max:100'
        ];
    }

    public function messages(){
        return [
            'name.max' => 'Nombre de partida debe contener como máximo 50',
            'name.unique' => 'Nombre de partida ya existe actualmente',
            'code.max' => 'Código de la partida debe contener como máximo 20',
            'code.unique' => 'Código de partida ya existe actualmente',
            'description.required' => 'Descripcion es requerida',
            'description.max' => 'Descripción debe contener como máximo 100'
        ];
    }
}
