<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules($user = [], $isUpdate = false)
    {
        
        if($isUpdate):
            return [
                'first_name' => 'required|max:50',
                'middle_name' => 'max:50',
                'first_surname' => 'required|max:50',
                'second_surname' => 'required|max:50',
                'email' => ['required','email',Rule::unique('users')->ignore($user->id)],
                'dni'=> ['required','min:4','max:20',Rule::unique('users')->ignore($user->id)],
                'telephone' => 'required|min:8|max:15',
                'direction'=> 'required|max:255',
                'image' => 'max:255',
                'status_id'=>'required',
                'area_id' => 'required'
            ];
        endif;

        return [
            'first_name' => 'required|max:50',
            'middle_name' => 'max:50',
            'first_surname' => 'required|max:50',
            'second_surname' => 'required|max:50',
            'email' => 'required|email|unique:users,email',
            'dni'=>'required|min:4|max:20|unique:users,dni',
            'telephone' => 'required|min:8|max:15',
            'direction'=> 'required|max:255',
            'image' => 'max:255',
            'status_id'=>'required',
            'area_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => 'Primer nombre es requerido',
            'first_name.max' => 'Primer debe contener como maximo 50',
            'middle_name.max' => 'Primer debe contener como maximo 50',
            'first_surname.required' => 'Primer apellido es requerido',
            'first_surname.max' => 'Primer apellido debe contener como maximo 50',
            'second_surname.required' => 'Primer apellido es requerido',
            'second_surname.max' => 'Primer apellido debe contener como maximo 50',
            'email.unique' => 'Correo ya existe actualmente',
            'email.required' => 'Correo es requerido',
            'email.email'=> 'Correo no es valido',
            'dni.required' => 'Cédula es requerida',
            'dni.min' => 'Cédula debe contener como minimo 4 caracteres',
            'dni.max' => 'Cédula debe contener como maximo 20 caracteres',
            'dni.unique' => 'Cédula ya existe actualmente',
            'telephone.required' => 'Teléfono es requerido',
            'telephone.min' => 'Teléfono debe contener como minimo 8 caracteres',
            'direction.required' => 'Dirección es requerida',
            'direction.max' => 'Dirección debe contener como maximo 255 caracteres',
            'image.max' => 'El nombre de la ruta de la imagen debe contener como maximo 255 caracteres',
            'status_id.required' => 'Estado es requerido',
            'area_id.required'=>'Departameto es requerido',
         ];
    }
}
