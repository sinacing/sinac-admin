<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AreaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules($area=[], $isUpdate = false)
    {
        if($isUpdate):
            return [
                'name'=> ['required','min:4',Rule::unique('areas')->ignore($area->id)],
                'location'=>'required|max:100|min:4',
                'telephone'=>'required|max:15|min:8',
                'description'=>'required|max:100|min:4'
            ]; 
        endif;
        return [
            'name'=> 'required|unique:areas,name|min:4',
            'location'=>'required|max:100|min:4',
            'telephone'=>'required|max:15|min:8',
            'description'=>'required|max:100|min:4'
        ];
    }

    public function messages(){
        return [
            'name.required' => 'Nombre es requerido',
            'name.max' => 'Nombre debe contener como máximo 50',
            'name.min' => 'Nombre debe contener como mínimo 4',
            'name.unique' => 'Nombre del departamento ya existe actualmente',
            'location.required' => 'La ubicación es requerida',
            'location.max' => 'El detalle de la ubicación debe contener como máximo 100',
            'location.min' => 'El detalle de la ubicación debe contener como mánimo 4',
            'telephone.required' => 'El teléfono es requerido',
            'telephone.max' => 'El teléfono debe contener como máximo 15',
            'telephone.min' => 'El telefono debe contener como mínimo 8',
            'description.required' => 'Descripción es requerida',
            'description.max' => 'Descripción debe contener como máximo 100',
            'description.max' => 'Descripción debe contener como mínimo 4',  
        ];
    }
}
