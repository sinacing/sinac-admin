<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ActiveUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules($active_user = [], $isUpdate = false)
    {
        return $isUpdate ? [
            'active_id' => ['required', Rule::unique('active_users')->ignore($active_user->active_user)]
        ] :[
           'active_id' => 'required|unique:active_users,active_id' 
        ];
    }

    public function messages(){
        return [
            'active_id.unique'=> 'Activo ya esta asignado a otro usuario'
        ];
    }
}
