<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class WarehouseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules($warehouse = null,$isUpdate = false)
    {
        if($isUpdate):
            return [
                'name' => ['max:50',Rule::unique('warehouses')->ignore($warehouse->id)],
                'code' => ['max:20',Rule::unique('warehouses')->ignore($warehouse->id)],
                'description' => 'required|max:100',
            ];
        endif;

        return [
            'name' => ['max:50','unique:warehouses,name'],
            'code' => ['max:20','unique:warehouses,code'],
            'description' => 'required|max:100',
        ];
    }

    public function messages(){
        return [
            'name.max' => 'Nombre de bodega debe contener como máximo 50',
            'name.unique' => 'Nombre de bodega ya existe actualmente',
            'code.max' => 'Código de bodega debe contener como máximo 20',
            'code.unique' => 'Código bodega ya existe actualmente',
            'description.required' => 'Descripcion es requerida',
            'description.max' => 'Descripción debe contener como máximo 100',
        ];
    }
}
