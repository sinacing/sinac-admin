<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuantityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'stock'=> 'required|min:0',
            'quanty'=> 'required|min:1',
            'total_quanty'=> 'required|min:0',
            'unit_measure_id' => 'required',
            'type_commodity_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'stock.required'=> 'Cantidad es requerida',
            'stock.min' => 'Cantidad debe ser mínimo 0',
            'quanty.required'=> 'Cantidad unitario del artículo es requerido',
            'quanty.min' => 'Cantidad unitario del artículo debe ser mínimo 1',
            'total_quanty.required'=> 'Cantidad total unitario del artículo es requerido',
            'total_quanty.min' => 'Cantidad total unitario del artículo debe ser mínimo 1',
            'unit_measure_id.required'=> 'Unidad es requerida',
            'type_commodity_id.required'=> 'Tipo es requerido',
        ];
    }
}
