<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ActiveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules($active = [], $isUpdate = false)
    {
       if($isUpdate):
        return [
            'code'=> ['required','max:30',Rule::unique('actives')->ignore($active->id)],
            'description' => 'max:100',
            'brand'=> 'required|min:2|max:50',
            'model'=> 'required|min:2|max:50',
            'value'=> 'required|regex:/^\d*(\.\d{2})?$/',
            //'user_id'=> 'required',
            //'area_id'=> 'required',
            'dependency'=> 'required|max:100',
            'status_id'=> 'required',
            'subcategory_id'=> 'required',
            'warehouse_id'=> 'required'
        ];
       endif;
       
       return [
        'code'=> 'required|max:30|unique:actives,code',
        'description' => 'max:100',
        'brand'=> 'required|min:2|max:50',
        'model'=> 'required|min:2|max:50',
        'value'=> 'required|regex:/^\d*(\.\d{2})?$/',
        //'user_id'=> 'required',
        //'area_id'=> 'required',
        'dependency'=> 'required|max:100',
        'status_id'=> 'required'
        ];
    }

    public function messages()
    {
        return [
            //requires
            'code.required'=>'Código es requerido',
            'code.unique' => 'Código ya existe actualmente',
            'brand.required'=> 'Marca es requerida',
            'model.required'=> 'Modelo es requerido',
            'value.required'=> 'Valor es requerido',
            'value.regex'=>'El formato del campo valor es invalido',
          //  'user_id.required'=> 'Usuario es requerido',
          //  'area_id.required'=> 'Area es requerida',
            'dependency.required'=> 'Dependencia es requerida',
            'status_id.required'=> 'Estado es requerido',
            // max and min 
            'code.max'=>'Código debe contener como máximo 30 caracteres',
            'description.max'=>'Descripción debe contener como máximo 30 caracteres',
            'brand.max'=>'Marca debe contener como máximo 50 caracteres',
            'brand.min'=>'Marca debe contener como mínimo 2 caracteres',
            'model.max'=>'Modelo debe contener como máximo 50 caracteres',
            'model.min'=>'Modelo debe contener como mínimo 2 caracteres',
            'dependency.max'=>'Dependencia debe contener como máximo 100 caracteres',
        ];
    }
}
