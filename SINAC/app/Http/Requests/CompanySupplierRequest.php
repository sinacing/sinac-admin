<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CompanySupplierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules($companySupplier = [], $isUpdate = false)
    {

        return $isUpdate ? [
            'dni'=>['required', Rule::unique('company_suppliers')->ignore($companySupplier->id)],
            'name'=> 'required|max:100',
            'telephone'=> 'required|max:15',
            'direction'=> 'required|max:255',
            'bank_account' => 'max:30',
            'status_id' => 'required'
        ] : [
            'dni'=>'required|unique:company_suppliers,dni',
            'name'=> 'required|max:100',
            'telephone'=> 'required|max:15',
            'direction'=> 'required|max:255',
            'bank_account' => 'max:30',
            'status_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'dni.required' => 'Cedula es requerida',
            'dni.unique' => 'Cedula ya existe actualmente',
            'name.required' => 'Nombre es requerido',
            'name.max' => 'Nombre sobre paso el maximo de 100 dijitos',
            'telephone.required'=> 'Telefono es requerido',
            'telephone.max' => 'Telefono a sobre pasado el maximo de 15 dijitos',
            'direction.required' => 'Direccion es requerida',
            'direction.max' => 'Direccion sobre paso el maximo de 255 dijitos',
            'bank_account' => 'Cuenta bancaria sobre paso el maximo de 30 caracteres',
            'status_id.required' => 'El estado es requerido'
        ];
    }
}
