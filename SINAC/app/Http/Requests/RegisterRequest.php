<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:50',
            'middle_name' => 'max:50',
            'first_surname' => 'required|max:50',
            'second_surname' => 'required|max:50',
            'email' => 'required|email|unique:registers,email',
            'dni'=>'required|min:4|max:20|unique:registers,dni|',
            'telephone' => 'required|min:8',
            'direction'=> 'required|max:255',
            'password' => 'required|min:6',
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => 'Primer nombre es requerido',
            'first_name.max' => 'Primer nombre debe contener como máximo 50',
            'middle_name.max' => 'Segundo nombre debe contener como máximo 50',
            'first_surname.required' => 'Primer apellido es requerido',
            'first_surname.max' => 'Primer apellido debe contener como máximo 50',
            'second_surname.required' => 'Primer apellido es requerido',
            'second_surname.max' => 'Primer apellido debe contener como máximo 50',
            'email.unique' => 'Correo ya existe actualmente',
            'email.required' => 'Correo es requerido',
            'email.email'=> 'Correo es inválido',
            'dni.required' => 'Cédula es requerida',
            'dni.min' => 'Cédula debe contener como mínimo 4 caracteres',
            'dni.max' => 'Cédula debe contener como máximo 20 caracteres',
            'dni.unique' => 'Cédula ya existe actualmente',
            'telephone.required' => 'Teléfono es requerido',
            'telephone.min' => 'Teléfono debe contener como mínimo 8 caracteres',
            'direction.required' => 'Dirección es requerida',
            'direction.max' => 'Dirección debe contener como máximo 255 caracteres',
            'password.required' => 'Contraseña requerida',
            'password.min' => 'Contraseña debe contener como mínimo 6 caracteres',
            'role_id.required'=> 'Los roles son requeridos'
         ];

    }
}
