<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CommodityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules($commodity=[],$isUpdate=false)
    {
        if($isUpdate):
            return [
                'code'=> ['required','max:20',Rule::unique('commodities')->ignore($commodity->id)],
                'name'=> ['required','max:50',Rule::unique('commodities')->ignore($commodity->id)],
                'price_unit' => 'required|regex:/^\d*(\.\d{2})?$/',
                'price_total'=> 'required|regex:/^\d*(\.\d{2})?$/',
                'subcategory_id'=> 'required',
                'warehouse_id'=> 'required',
                'status_id'=> 'required',
                'quantity_id' =>'required'
            ];
        endif;

        return [
            'code'=> 'required|max:20|unique:commodities,code',
            'name'=> 'required|max:50|unique:commodities,name',
            'price_unit' => 'required|regex:/^\d*(\.\d{2})?$/',
            'price_total'=> 'required|regex:/^\d*(\.\d{2})?$/',
            'subcategory_id'=> 'required',
            'warehouse_id'=> 'required',
            'status_id'=> 'required',
            'quantity_id' =>'required'

        ];
    }

    public function messages()
    {
        return [
            //requires
            'code.required'=>'Código es requerido',
            'code.unique'=>'Código ya existe actualmente',
            'name.required'=>'Nombre es requerido',
            'name.unique' => 'Nombre ya existe actualmente',
            'price_unit.required'=> 'Precio unidad es requerida',
            'price_total.required'=> 'Precio total es requerido',
            'subcategory_id.required'=> 'Subpartida es requerida',
            'warehouse_id.required'=> 'Departamento es requerido',
            'status_id.required'=> 'Estado es requerido',
            'quantity_id.required' =>'El id de cantidad es requerido',

            // max and min regex
            'code.max'=>'Código debe contener como máximo 20 caracteres',
            'name.max'=>'Nombre debe contener como máximo 50 caracteres',
            'price_unit.regex'=>'El formato del campo precio unidad es inválido',
            'price_total.regex'=>'El formato del campo precio total es inválido',
        ];
    }
}
