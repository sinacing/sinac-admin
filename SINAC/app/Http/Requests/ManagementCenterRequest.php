<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ManagementCenterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules($managementCenter = [], $isUpdate = false)
    {
        return $isUpdate ? [
            'name' => ['required', 'max:50', Rule::unique('management_centers')->ignore($managementCenter->id)],
            'code' => ['required', 'max:20', Rule::unique('management_centers')->ignore($managementCenter->id)]
        ] :[
            'name' => 'required|max:50|unique:management_centers,name',
            'code' => 'required|max:20|unique:management_centers,code'
        ];
    }

    public function messages(){
        return [
            'name.required' => 'Nombre es requerido',
            'name.max' => 'Nombre de centro gestor debe contener como máximo 50',
            'name.unique' => 'Nombre de centro gestor ya existe actualmente',
            'code.required' => 'Código es requerido',
            'code.max'=> 'Código de centro gestor debe contener como máximo 20',
            'code.unique' => 'Código de centro gestor ya existe actualmente'
        ];
    }
}
