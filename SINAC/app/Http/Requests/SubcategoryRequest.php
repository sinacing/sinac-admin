<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SubcategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules($Subcategory = null,$isUpdate = false)
    {
        if($isUpdate):
            return [
                'name' => ['max:50',Rule::unique('subcategories')->ignore($Subcategory->id)],
                'code' => ['max:20',Rule::unique('subcategories')->ignore($Subcategory->id)],
                'description' => 'required|max:100',
                'category_id' => 'required'
            ];
        endif;

        return [
            'name' => ['max:50','unique:subcategories,name'],
            'code' => ['max:20','unique:subcategories,code'],
            'description' => 'required|max:100',
            'category_id' => 'required'
        ];
    }

    public function messages(){
        return [
            'name.max' => 'Nombre de subpartida debe contener como máximo 50',
            'name.unique' => 'Nombre de subpartida ya existe actualmente',
            'code.max' => 'Código de subpartida debe contener como máximo 20',
            'description.required' => 'Descripcion es requerida',
            'code.unique' => 'Código de subpartida ya existe actualmente',
            'description.max' => 'Descripción debe contener como máximo 100',
            'category_id.required' => 'Partida es requerida'
        ];
    }
}
