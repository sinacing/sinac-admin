<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PersonSupplierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules($personSupplier = [], $isUpdate = false)
    {
        return $isUpdate ? [
            'dni' => ['max:20',Rule::unique('person_suppliers')->ignore($personSupplier->id)],
            'first_name' => 'required|max:50',
            'middle_name' => 'max:50',
            'first_surname' => 'required|max:50',
            'second_surname' => 'required|max:50',
            'telephone' => 'required|max:15',
            'direction' => 'required|max:255',
            'bank_account' => 'required|max:30',
            'status_id' => 'required' 
        ] : [
            'dni' => 'max:20|unique:person_suppliers,dni',
            'first_name' => 'required|max:50',
            'middle_name' => 'max:50',
            'first_surname' => 'required|max:50',
            'second_surname' => 'required|max:50',
            'telephone' => 'required|max:15',
            'direction' => 'required|max:255',
            'bank_account' => 'required|max:30',
            'status_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'dni.max' => 'Cédula no debe sobrepasar los 50 caracteres',
            'dni.unique' => 'Cédula ya existe actualmente',
            'dni.required' => 'Cédula es requerida',
            'first_name.max' => 'Primer nombre no debe sobrepasar los 50 caracteres',
            'first_name.required' => 'Primer nombre es requerido',
            'middle_name.max' => 'Segundo nombre no debe sobrepasar los 50 caracteres',
            'first_surname.required' => 'Primer apellido es requerido',
            'first_surname.max' => 'Primer apellido no debe sobrepasar los 50 caracteres',
            'second_surname.required' => 'Segundo apellido es requerido',
            'second_surname.max' => 'Segundo apellido no debe sobrepasar los 50 caracteres',
            'telephone.required' => 'Teléfono es requerido',
            'telephone.max' => 'Teléfono no debe sobrepasar los 15 caracteres',
            'direction.required' => 'Dirección es requerido',
            'direction.max' => 'Dirección no debe sobrepasar los 255 caracteres',
            'bank_account.required' => 'Cuenta bancaria es requerida',
            'bank_account.max' => 'Cuenta bancaria no debe sobrepasar los 30 caracteres',
            'status_id.required' => 'Estado es requerido'
        ];
    }
}
