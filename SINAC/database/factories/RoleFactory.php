<?php

use Faker\Generator as Faker;

$factory->define(App\Role::class, function (Faker $faker) {
    return [
        'type'=>null,
        'description'=>$faker->text(50)
    ];
});
