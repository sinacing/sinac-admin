<?php

use App\Area;
use App\Status;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use App\User;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'first_name' => $faker->name,
        'middle_name' => $faker->name,
        'first_surname' => $faker->lastName,
        'second_surname' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'dni' => $faker->randomNumber().$faker->randomLetter,
        'password' => 'secret',
        'telephone' => $faker->randomNumber(),
        'direction' => $faker->sentence,
        'image' => '/images/user/default.jpg',
        'status_id' => Status::all()->random()->id,
        'area_id' => Area::all()->random()->id
    ];
});
