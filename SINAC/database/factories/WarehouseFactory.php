<?php

use Faker\Generator as Faker;

$factory->define(App\Warehouse::class, function (Faker $faker) {
    return [
        'name'=> $faker->name,
        'code'=> $faker->randomNumber().$faker->randomLetter,
        'description'=> $faker->text(50)
    ];
});
