<?php

use Faker\Generator as Faker;
use App\Status;

$factory->define(App\CompanySupplier::class, function (Faker $faker) {
    return [
        'dni' => $faker->randomNumber().$faker->randomLetter,
        'name' => $faker->company,
        'telephone' => $faker->randomNumber(),
        'direction' => $faker->sentence,
        'bank_account' => $faker->randomNumber(3).'-'.$faker->randomNumber(2).'-'.$faker->randomNumber(5),
        'status_id'=>Status::all()->random()->id,
    ];
});
