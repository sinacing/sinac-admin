<?php

use App\Status;
use Faker\Generator as Faker;

$factory->define(App\PersonSupplier::class, function (Faker $faker) {
    return [
        'dni' => $faker->randomNumber().$faker->randomLetter,
        'first_name' => $faker->name,
        'middle_name' => $faker->name,
        'first_surname' => $faker->lastName,
        'second_surname' => $faker->lastName,
        'telephone' => $faker->randomNumber(),
        'direction' => $faker->sentence,
        'bank_account' => $faker->randomNumber(3).'-'.$faker->randomNumber(2).'-'.$faker->randomNumber(5),
        'status_id'=>Status::all()->random()->id,
    ];
});
