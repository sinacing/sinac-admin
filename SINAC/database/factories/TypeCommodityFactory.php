<?php

use Faker\Generator as Faker;

$factory->define(App\TypeCommodity::class, function (Faker $faker) {
    return [
        'name'=>$faker->text(10)
    ];
});
