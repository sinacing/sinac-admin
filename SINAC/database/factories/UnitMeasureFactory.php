<?php

use Faker\Generator as Faker;

$factory->define(App\UnitMeasure::class, function (Faker $faker) {
    return [
        'name'=>$faker->randomLetter.$faker->randomLetter
    ];
});
