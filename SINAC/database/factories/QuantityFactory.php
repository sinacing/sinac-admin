<?php

use Faker\Generator as Faker;
use App\UnitMeasure;
use App\TypeCommodity;

$factory->define(App\Quantity::class, function (Faker $faker) {
    $stock = $faker->numberBetween(1,50);
    $amout = $faker->randomNumber(4);

    return [
        'stock'=> $stock,
        'quanty'=> $amout,
        'total_quanty'=> $stock * $amout,
        'unit_measure_id'=>UnitMeasure::all()->random()->id,
        'type_commodity_id'=> TypeCommodity::all()->random()->id,
    ];
    
});
