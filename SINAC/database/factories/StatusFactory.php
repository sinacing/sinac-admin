<?php

use Faker\Generator as Faker;
use App\Status;

$factory->define(Status::class, function (Faker $faker) {
    return [
        'type'=>$faker->name,
        'description'=>$faker->sentence()
    ];
});
