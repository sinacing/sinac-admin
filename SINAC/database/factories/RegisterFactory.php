<?php

use App\Role;
use Faker\Generator as Faker;

$factory->define(App\Register::class, function (Faker $faker) {
    return [
        'first_name' => $faker->name,
        'middle_name' => $faker->name,
        'first_surname' => $faker->lastName,
        'second_surname' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'dni' => $faker->randomNumber().$faker->randomLetter,
        'password' => 'secret',
        'telephone' => $faker->randomNumber(),
        'direction' => $faker->sentence,
        'role_id' => Role::all()->random()->id 
    ];
});
