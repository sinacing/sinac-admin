<?php

use Faker\Generator as Faker;

use App\Category;

$factory->define(App\Subcategory::class, function (Faker $faker) {
    return [
        'name'=> $faker->name,
        'code'=> $faker->randomNumber().$faker->randomLetter,
        'description'=> $faker->text(50),
        'category_id' => Category::all()->random()->id
    ];
});

