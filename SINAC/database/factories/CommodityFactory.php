<?php

use App\Quantity;
use App\Status;
use App\Subcategory;
use App\Warehouse;
use Faker\Generator as Faker;

$factory->define(App\Commodity::class, function (Faker $faker) {
    $quantity = Quantity::all()->random();
    $unitPrice = $faker->numberBetween(100,200000);

    return [
        'code'=>$faker->randomNumber(). $faker->randomLetter,
        'name'=>$faker->text(20),
        'price_unit'=>$unitPrice,
        'price_total'=>$unitPrice * $quantity->stock,
        'subcategory_id'=>Subcategory::all()->random()->id,
        'status_id'=>Status::all()->random()->id,
        'warehouse_id'=>Warehouse::all()->random()->id,
        'quantity_id'=>$quantity->id,
    ];
});
