<?php

use Faker\Generator as Faker;

$factory->define(App\ManagementCenter::class, function (Faker $faker) {
    return [
        'name'=> $faker->company,
        'code'=> $faker->randomNumber().$faker->randomLetter
    ];
});
