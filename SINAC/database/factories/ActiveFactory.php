<?php

use App\Status;
use Faker\Generator as Faker;
use App\Warehouse;
use App\Subcategory;

$factory->define(App\Active::class, function (Faker $faker) {
    return [
        'code'=>$faker->randomNumber().$faker->randomLetter,
        'description'=>$faker->sentence(),
        'brand' => $faker->text(10),
        'model' => $faker->text(20),
        'value' => $faker->randomFloat(2,5,10),
        'dependency' => $faker->text(50),
        'status_id'  => Status::all()->random()->id,
        'subcategory_id' => Subcategory::all()->random()->id,
        'warehouse_id' => Warehouse::all()->random()->id
    ];
});
