<?php

use Faker\Generator as Faker;

$factory->define(App\Area::class, function (Faker $faker) {
    return [
        'name' => null,
        'location'=> $faker->text(50),
        'telephone' => $faker->randomNumber(),
        'description'=> $faker->sentence()
    ];
});
