<?php

use App\Area;
use App\Role;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 50);
            $table->string('middle_name', 50)->nullable();
            $table->string('first_surname', 50);
            $table->string('second_surname', 50);
            $table->string('email')->unique();
            $table->string('dni', 20)->unique();
            $table->string('password',255)->default();
            $table->string('telephone', 15);
            $table->string('direction')->nullable();
            $table->unsignedInteger('area_id')->default(Area::defaultArea)->nullable();
            $table->unsignedInteger('role_id')->default(Role::administratorEmployee)->nullable();
            $table->foreign('role_id')->references('id')->on('roles');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registers');
    }
}
