<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuantitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quantities', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('stock');
            $table->unsignedInteger('quanty');
            $table->unsignedInteger('total_quanty');
            $table->unsignedInteger('unit_measure_id');
            $table->foreign('unit_measure_id')->references('id')->on('unit_measures');
            $table->unsignedInteger('type_commodity_id');
            $table->foreign('type_commodity_id')->references('id')->on('type_commodities');
            $table->timestamps();
        });
    }
   
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quantities');
    }
}
