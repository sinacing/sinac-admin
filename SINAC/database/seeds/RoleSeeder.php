<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Role::class,1)->create([
            'type'=> 'Admnistrator'
        ]);
        factory(Role::class,1)->create([
            'type'=> 'Employee'
        ]);
        
        factory(Role::class,1)->create([
            'type'=> 'Mercaderia'
        ]);

        factory(Role::class,1)->create([
            'type'=> 'Vehiculos'
        ]);

        factory(Role::class,1)->create([
            'type'=> 'Activos'
        ]);

        factory(Role::class,1)->create([
            'type'=> 'Presupuesto'
        ]);

    }
}
