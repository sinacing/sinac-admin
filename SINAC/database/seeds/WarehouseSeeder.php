<?php

use Illuminate\Database\Seeder;
use App\Warehouse;

class WarehouseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Warehouse::class)->create([
            'name' => 'Bodega ryu'
        ]);

        factory(Warehouse::class)->create([
            'name' => 'Bodega chanfa'
        ]);

        factory(Warehouse::class)->create([
            'name' => 'Bodega cucuru'
        ]);
    }
}
