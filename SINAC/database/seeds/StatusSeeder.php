<?php

use App\Status;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Status::class,1)->create([
            'type'=>'Activo',
            'description'=>'Active object',
        ]);

        factory(Status::class,1)->create([
            'type'=>'Inactivo',
            'description'=>'No-Active object',
        ]);

        factory(Status::class,1)->create([
            'type'=>'Bueno',
            'description'=>'Good object',
        ]);

        factory(Status::class,1)->create([
            'type'=>'Regular',
            'description'=>'Regular object',
        ]);

        factory(Status::class,1)->create([
            'type'=>'Malo',
            'description'=>'Bad object',
        ]);
    }
}
