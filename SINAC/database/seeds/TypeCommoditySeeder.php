<?php

use App\TypeCommodity;
use Illuminate\Database\Seeder;

class TypeCommoditySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(TypeCommodity::class)->create([ 'name'=>'Galones' ]);
        factory(TypeCommodity::class)->create([ 'name'=>'Cajas' ]);
        factory(TypeCommodity::class)->create([ 'name'=>'Sobres' ]);
        factory(TypeCommodity::class)->create([ 'name'=>'Estañon' ]);        

    }
}
