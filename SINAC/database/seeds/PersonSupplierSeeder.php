<?php

use App\PersonSupplier;
use Illuminate\Database\Seeder;

class PersonSupplierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(PersonSupplier::class,5)->create();
    }
}
