<?php

use Illuminate\Database\Seeder;
use App\Subcategory;
class SubcategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Subcategory::class)->create([
            'name'=>'Limpieza'
        ]);

        factory(Subcategory::class)->create([
            'name'=>'Hojas'
        ]);

        factory(Subcategory::class)->create([
            'name'=>'Combustibles'
        ]);

        factory(Subcategory::class)->create([
            'name'=>'Lapices'
        ]);

        factory(Subcategory::class)->create([
            'name'=>'Sobres'
        ]);

        factory(Subcategory::class)->create([
            'name'=>'Higienicos'
        ]);

        factory(Subcategory::class)->create([
            'name'=>'Lapiceros'
        ]);

        factory(Subcategory::class)->create([
            'name'=>'Jabones'
        ]);

        factory(Subcategory::class)->create([
            'name'=>'No se me ocurre nada'
        ]);
    }
}
