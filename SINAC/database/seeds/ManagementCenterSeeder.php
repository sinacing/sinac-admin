<?php

use App\ManagementCenter;
use Illuminate\Database\Seeder;

class ManagementCenterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(ManagementCenter::class,5)->create();
    }
}
