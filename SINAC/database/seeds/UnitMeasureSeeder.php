<?php

use App\UnitMeasure;
use Illuminate\Database\Seeder;

class UnitMeasureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(UnitMeasure::class)->create([ 'name'=>'kl' ]);
        factory(UnitMeasure::class)->create([ 'name'=>'kg' ]);
        factory(UnitMeasure::class)->create([ 'name'=>'km' ]);
        factory(UnitMeasure::class)->create([ 'name'=>'ml' ]);
        factory(UnitMeasure::class)->create([ 'name'=>'mg' ]);
        factory(UnitMeasure::class)->create([ 'name'=>'m' ]);
        factory(UnitMeasure::class)->create([ 'name'=>'l' ]);
        factory(UnitMeasure::class)->create([ 'name'=>'hl' ]);
        factory(UnitMeasure::class)->create([ 'name'=>'dam' ]);
        factory(UnitMeasure::class)->create([ 'name'=>'g' ]);
        factory(UnitMeasure::class)->create([ 'name'=>'dm' ]);
        factory(UnitMeasure::class)->create([ 'name'=>'Unidad']);

    }   
}
