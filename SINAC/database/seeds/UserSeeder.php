<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\support\facades\Hash;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'first_name' => 'Joseph',
            'middle_name' => '',
            'first_surname' => 'Ramirez',
            'second_surname' => 'Marchena',
            'email' => 'jarm061198@gmail.com',
            'email_verified_at' => now(),
            'dni' => '117260018',
            'password' => 'secret',
            'telephone' => '45678221',
            'direction' => 'Granja',
            'image' => '/default/1/2',
        ]);

        factory(User::class)->create([
            'first_name' => 'Yarelis',
            'middle_name' => '',
            'first_surname' => 'gomez',
            'second_surname' => 'jimemez',
            'email' => 'yare@gmail.com',
            'email_verified_at' => now(),
            'dni' => '504260092',
            'password' => 'secret',
            'telephone' => '88888888',
            'direction' => 'Belen',
            'image' => '/default/1/2',
        ]);
        
        factory(User::class)->create([
            'first_name' => 'Josue',
            'middle_name' => '',
            'first_surname' => 'Vasquez',
            'second_surname' => 'Sequira',
            'email' => 'jossuseque@hotmail.com',
            'email_verified_at' => now(),
            'dni' => '504240573',
            'password' => 'secret',
            'telephone' => '86484271',
            'direction' => 'Cacao', 
            'image' => '/default/1/2',
        ]);

        factory(User::class)->create([
            'first_name' => 'Chelsy',
            'middle_name' => 'Paola',
            'first_surname' => 'Paniagua',
            'second_surname' => 'Garcia', 
            'email' => 'chelsy@hotmail.com',
            'email_verified_at' => now(),
            'dni' => '504150562',
            'password' =>'secret',
            'telephone' => '84472065',
            'direction' => 'Estrada', 
            'image' => '/default/1/2',
        ]);

        factory(User::class,15)->create();
    }
}
