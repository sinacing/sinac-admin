<?php

use App\CompanySupplier;
use Illuminate\Database\Seeder;

class CompanySupplierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(CompanySupplier::class,5)->create();
    }
}
