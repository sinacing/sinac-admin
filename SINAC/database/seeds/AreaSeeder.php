<?php

use Illuminate\Database\Seeder;
use App\Area;

class AreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Area::class)->create([
            'name'=>'Nicoya'
        ]);

        factory(Area::class,1)->create([
            'name'=> 'Barra Honda'
        ]);

        factory(Area::class,1)->create([
            'name'=> 'Hojancha'
        ]);

        factory(Area::class,1)->create([
            'name'=> 'Camaronal'
        ]);

        factory(Area::class,1)->create([
            'name'=> 'Conchal'
        ]);

        factory(Area::class,1)->create([
            'name'=> 'Marino las Baulas'
        ]);

        factory(Area::class,1)->create([
            'name'=> 'Cabo Blanco'
        ]);

        factory(Area::class,1)->create([
            'name'=> 'Ostional'
        ]);
    }
}
