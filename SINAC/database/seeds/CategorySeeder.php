<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Category::class)->create([
            'name'=> 'Partida 1',
        ]);

        factory(Category::class)->create([
            'name'=> 'Partida 2',
        ]);

        factory(Category::class)->create([
            'name'=> 'Partida 3',
        ]);

        factory(Category::class)->create([
            'name'=> 'Partida 4',
        ]);
    }
}
